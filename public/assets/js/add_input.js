var target = $("div#target");
var n = function() {
  return $("div.col-lg-2").length;
};
var newInput = function() {
  var div = $("<div/>", {
    "class": "input-group"
  });
  var input = $("<input>", {
    "class": "form-control"
  });
  var span = $("<span/>", {
    "class": "input-group-btn"
  });
  var button = $("<button/>", {
    "class": "removeBtn",
    type: "button",
    id: n()
  });
  var glyph = $("<span/>", {
    "class": "fa fa-times"
  });
  var col = $("<div/>", {
    "class": "form-group col-lg-12",
    id: "newInput-" + n()
  });

  $(glyph).appendTo(button);
  $(button).appendTo(span);
  $(input).appendTo(div);
  $(span).appendTo(div);
  $(div).appendTo(col);
  return col;
};

$('button#add').on('click', function() {
  $(newInput()).appendTo(target);
});

$('#target').on('click', 'button', function() {
  var target = $("#target").find("#newInput-" + this.id);
  $(target).remove();
});