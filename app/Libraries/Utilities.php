<?php

namespace App\Libraries;

/**
 * Common Utility Functions
 */

class Utilities
{
	
	function generateOTP(){
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 

	    return $otp;
	}

	
}