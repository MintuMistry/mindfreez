<div id="fullbox">
<div class="qr-code-img drop-star">
	<div class="container">
				<div class="all-start">
					<div><img src="<?php echo base_url(); ?>/assets/images/1.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/2.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/3.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/4.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/5.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/6.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/7.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/8.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/1.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/2.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/3.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/4.png"></div>
					<div><img src="<?php echo base_url(); ?>/assets/images/5.png"></div>
				</div>
		
		<div class="row vertical-100">
		<?php
		//echo "<pre>";
		//print_r($team_result);
		//die();
		foreach($team_result as $result){
			
			if($result['rank'] == 2)
			{
		?>
			<div class="col-lg-3 align-self-center">
				<div class="runners_up">
					<h1>Runner's Up</h1>
					<h5 class="" data-toggle="modal" data-target=".runner-modal"><?=$result['team_name']?> (<?=$result['tpoints']?> Points)</h5>
				</div>
			</div>
		<?php }
		
		if($result['rank'] == 1){ ?>
			
			<div class="col-lg-6 align-self-center">
				<div class="winner">
					<h1>Winner</h1>
					<h3 class="" data-toggle="modal" data-target=".winner-modal"><?=$result['team_name']?> (<?=$result['tpoints']?> Points)</h3>
				</div>
				<div class="winner_Cup">
					<img src="<?php echo base_url(); ?>/assets/images/team_cup.png" alt="images" class="img-fluid">
				</div>
			</div>
			<?php }
			
			if($result['rank'] == 3){ ?>
			
			<div class="col-lg-3 align-self-center">
				<div class="runners_up">
					<h1>2nd Runner's Up</h1>
					<h5 class="" data-toggle="modal" data-target=".bd-example-modal-lg"><?=$result['team_name']?> (<?=$result['tpoints']?> Points)</h5>
				</div>
			</div>
			<?php }}?>
		</div>
	
	</div>
<!----------------- Winner --------------->

<div class="modal fade winner-modal winner-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content modal-style">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="container">
       		<div class="row">
       			<div class="col-lg-5">
       				<div class="sild-img"><img src="<?php echo base_url(); ?>/assets/images/award.png" alt="images" class="img-fluid"></div>
       			</div>
       			<div class="col-lg-7">
				<?php
				foreach($team_result as $result){
					
					if($result['rank'] == 1)
					{
				?>
       				<div class="winner-details">
       					<h1> Winner </h1>
       					<h3> <?=$result['team_name']?> </h3>
						<?php
						$i=0;
						foreach($result['user_result'] as $user_result){
							$i++;
							?>
						
       					<div class="winner-number"><span class="winner-team-memeber"> <?=$i?>. </span> <?=$user_result['full_name']?> </div>
						<?php } ?>
       				</div>
				<?php }}?>
       			</div>
       		</div>
       </div>
      </div>
    </div>
  </div>
</div>
<!----------------- End Winner --------------->
<!----------------- 1st Runners --------------->

<div class="modal fade runner-modal winner-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content modal-style">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="container">
       		<div class="row">
       			<div class="col-lg-5">
       				<div class="sild-img"><img src="<?php echo base_url(); ?>/assets/images/award.png" alt="images" class="img-fluid"></div>
       			</div>
       			<div class="col-lg-7">
				<?php
				foreach($team_result as $result){
					
					if($result['rank'] == 2)
					{
				?>
       				<div class="winner-details">
       					<h1> 1st Runners </h1>
       					<h3> <?=$result['team_name']?> </h3>
						<?php
						$i=0;
						foreach($result['user_result'] as $user_result){
							$i++;
							?>
						
       					<div class="winner-number"><span class="winner-team-memeber"> <?=$i?>. </span> <?=$user_result['full_name']?> </div>
						<?php } ?>
       				</div>
					<?php }}?>
       			</div>
       		</div>
       </div>
      </div>
    </div>
  </div>
</div>
<!----------------- End 1st Runners --------------->
<!----------------- 2nd Runners --------------->
<div class="modal fade bd-example-modal-lg winner-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content modal-style">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="container">
       		<div class="row">
       			<div class="col-lg-5">
       				<div class="sild-img"><img src="<?php echo base_url(); ?>/assets/images/award.png" alt="images" class="img-fluid"></div>
       			</div>
       			<div class="col-lg-7">
				<?php
				foreach($team_result as $result){
					
					if($result['rank'] == 3)
					{
				?>
       				<div class="winner-details">
       					<h1> 2nd Runners </h1>
       					<h3> <?=$result['team_name']?> </h3>
						<?php
						$i=0;
						foreach($result['user_result'] as $user_result){
							$i++;
							?>
						
       					<div class="winner-number"><span class="winner-team-memeber"> <?=$i?>. </span> <?=$user_result['full_name']?> </div>
						<?php } ?>
       				</div>
					<?php }}?>
       			</div>
       		</div>
       </div>
      </div>
    </div>
  </div>
</div>
<!----------------- End 2nd Runners --------------->


</div>

</div>