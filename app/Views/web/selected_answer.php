<?php
//echo "<pre>";
//print_r($question_list);
$i=0;
foreach($question_list as $question)
{
?>
<div id="fullbox">
	<div class="video-question-box">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<div class="header-video-bg">
						<video class="bg-video" poster="<?php echo base_url()."/writable/uploads/".$question['filepath'];?>" autoplay="" loop="" muted="">
							<source src="<?php echo base_url(); ?>/assets/video/vid.mp4">
						</video>
					</div>
					<div class="logo"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo" class="img-fluid"></div>
				</div>
				<div class="col-lg-6 p-0">
					<div id="que_ans_sec">
						<div class="question">
							<p> <?=$question['question_text']?></p>
						</div>
						<?php
						$alphabet =array("A","B","C","D","E","F","G","H");
						$j = 0;
						$next_ques = $question[$i+1]['question_id'];
						$poll_arr = array();
						foreach($question['options'] as $option)
						{
							$poll_arr[] = $option['poll'];
						}
						//print_r($poll_arr);
						foreach($question['options'] as $option)
						{
						?>
						<a href="<?php echo base_url();?>/admin/home/<?=$question['gameid']."@".$question['nextquesid'];?>"><div class="ans-part"><span class="que_num"><?=$alphabet[$j]?></span><span class="ans-box"><?=$option['option_text']?></span>
				<?php if($option['poll'] == max($poll_arr)){?>
						<span class="correct_ans"><img src="<?php echo base_url();?>/assets/images/right_ans.png"></span>
				<?php }else{ ?>
						<span class="wrong_ans"><img src="<?php echo base_url();?>/assets/images/wrong_ans.png"></span>
				<?php } ?>
						</div></a>
						<?php $j++; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="percent-sec">
		<div class="container-fluid">
			<div class="row">
			<?php
			$j = 0;
			$alphabet =array("A","B","C","D","E","F","G","H");
			$next_ques = $question[$i+1]['question_id'];
			$poll_arr = array();
			foreach($question['options'] as $option)
			{
				$poll_arr[] = $option['poll'];
			}
			//print_r($poll_arr);
			$color_arr = array("color-one","color-two","color-three","color-four");
			foreach($question['options'] as $option)
			{
			?>
				<div class="col-lg-3">
					<div class="percentag-box">
						<div class="progress-bar-num <?=$color_arr[$j]?>"><?=$alphabet[$j]?></div>
						<div class="progress">
							<div class="progress-percentage" style="width:<?=$poll_arr[$j]?>%; text-align: right;"><?=$poll_arr[$j]?>%</div>
						  <div class="progress-bar color-one" role="progressbar" style="width: <?=$poll_arr[$j]?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			<?php $j++; } ?>
			</div>
		</div>
	</div>

	<div id="footerpart">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="new-quesiton">
						<div class="row">
							<div class="col-lg-6"> 
								<?=$question['explanation']?>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>
</div>
<?php $i++; } ?>
<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>