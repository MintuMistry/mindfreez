<div id="fullbox">
	<div class="video-question-box">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<div class="header-video-bg">
						<video class="bg-video" poster="<?php echo base_url(); ?>/assets/images/bg/1.jpeg" autoplay="" loop="" muted="">
							<source src="<?php echo base_url(); ?>/assets/video/vid.mp4">
						</video>
					</div>
					<div class="logo"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo" class="img-fluid"></div>
				</div>
				<div class="col-lg-6 p-0">
			<a href="http://yesitlabs.xyz/designs/MindFreeze_websites_html/winning_team.html">
					<div id="que_ans_sec">
						<div class="question mb-4">
							<p> Finish this verse: "I said a hip hop"...</p>
						</div>
						<div class="ans-part true-false-box right_question"><span class="que_num">A</span><span class="truefalse-box"> True </span><span class="correct_ans"><img src="images/right_ans.png"></span></div>
						<div class="ans-part true-false-box"><span class="que_num">B</span><span class="truefalse-box"> False </span><span class="wrong_ans"><img src="images/wrong_ans.png"></span></div>
						
						<div class="box-progress">
							<div class="row">
								<div class="col-lg-6">
									<div class="percentag-box">
										<div class="progress-bar-num color-one">A</div>
										<div class="progress">
											<div class="progress-percentage" style="width:90%; text-align: right;">90%</div>
										  <div class="progress-bar color-one" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="percentag-box">
										<div class="progress-bar-num color-two">B</div>
										<div class="progress">
										  <div class="progress-percentage" style="width:80%; text-align: right;">80%</div>
										  <div class="progress-bar color-two" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
				</div>
			</div>
		</div>
	</div>

	<div id="footerpart" class="true_false">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4">
					<div class="new-quesiton">
						<div class="row align-items-center">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="images/footer_question.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Question</div>
									<div class="point-numb">01</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="new-clock">
						<div class="row align-items-center">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="images/footer_clock.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Clock</div>
									<div class="point-numb">0:20</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="new-points">
						<div class="row align-items-center">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="images/footer_point.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Points</div>
									<div class="point-numb">100</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>