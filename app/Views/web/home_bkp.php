<?php
//echo "<pre>";
//print_r($question_list);
$i=0;
foreach($question_list as $question)
{
?>
<div id="fullbox<?=$i?>" style="display:none;">
	<div class="video-question-box" >
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 p-0">
					<div class="header-video-bg">
						<video class="bg-video" poster="<?php echo base_url()."/writable/uploads/".$question['filepath'];?>" autoplay="" loop="" muted="">
							<source src="<?php echo base_url(); ?>/assets/video/vid.mp4">
						</video>
					</div>
					<div class="logo"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo" class="img-fluid"></div>
				</div>
				
				
				<div class="col-lg-6 p-0">
					<div id="que_ans_sec">
						<div class="question">
							<p> <?=$question['question_text']?></p>
						</div>
						<?php
						$alphabet =array("A","B","C","D","E","F","G","H");
						$j = 0;
						$next_ques = $question[$i+1]['question_id'];
						foreach($question['options'] as $option)
						{
						?>
						<a href="<?php echo base_url();?>/admin/checkAnswer/<?=$question['question_id']."@".$next_ques;?>"><div class="ans-part"><span class="que_num"><?=$alphabet[$j]?></span><span class="ans-box"><?=$option['option_text']?></span></div></a>
						<?php $j++; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="percent-sec">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<div class="percentag-box">
						<div class="progress-bar-num color-one">A</div>
						<div class="progress">
							<div class="progress-percentage" style="width:0%; text-align: right;">0%</div>
						  <div class="progress-bar color-one" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="percentag-box">
						<div class="progress-bar-num color-two">B</div>
						<div class="progress">
						  <div class="progress-percentage" style="width:0%; text-align: right;">0%</div>
						  <div class="progress-bar color-two" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="percentag-box">
						<div class="progress-bar-num color-three">C</div>
						<div class="progress">
							 <div class="progress-percentage" style="width:0%; text-align: right;">0%</div>
						  <div class="progress-bar color-three" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="percentag-box">
						<div class="progress-bar-num color-four">D</div>
						<div class="progress">
							 <div class="progress-percentage" style="width:0%; text-align: right;">0%</div>
						  <div class="progress-bar color-four" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="footerpart">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4">
					<div class="new-quesiton">
						<div class="row">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="<?php echo base_url(); ?>/assets/images/footer_question.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Question</div>
									<div class="point-numb">01</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="new-clock">
						<div class="row">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="<?php echo base_url(); ?>/assets/images/footer_clock.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Clock</div>
									<div class="point-numb count">
									<?php 
									
									list($hour,$min,$sec) = explode(':', '00:00:'.$question['time_limit']);
									$dbSessionDurationTime = mktime(0,0,0,$hour,$min,$sec);
									
									?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="new-points">
						<div class="row">
							<div class="col-lg-6"> 
								<div class="foot-img-style text-right"><img src="<?php echo base_url(); ?>/assets/images/footer_point.png" alt="images" class="img-fluid"></div></div> 
							<div class="col-lg-6">
								<div class="pointbox">
									<div class="point">Points</div>
									<div class="point-numb">100</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $i++; } ?>
<script>
window.addEventListener('load', (event) => {
	document.getElementById("fullbox0").style.display = "block"; 
	console.log('page is fully loaded');
});
</script>
<script type="text/javascript">
    var millis = <?php echo $dbSessionDurationTime; ?>
	var page_redirecttime = millis*1000;
    function displaytimer(){
        var hours = Math.floor(millis / 36e5),
            mins = Math.floor((millis % 36e5) / 6e4),
            secs = Math.floor((millis % 6e4) / 1000);
            //Here, the DOM that the timer will appear using jQuery
            //$('.count').html(hours+':'+mins+':'+secs);  
            $('.count').html(secs);
    }

    setInterval(function(){
        millis -= 1000;
        displaytimer();
    }, 1000);
setTimeout("location.href = '<?php echo base_url();?>';",page_redirecttime);
</script>
