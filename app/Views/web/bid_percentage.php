<div class='video-question-box'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-12 p-0'>
					<div class='header-video-bg'>
						<video class='bg-video' poster='<?php echo base_url(); ?>/assets/images/bg/1.jpeg' autoplay='' loop='' muted=''>
							<source src='<?php echo base_url(); ?>/assets/video/vid.mp4'>
						</video>
					</div>
					<div class='logo'><img src='<?php echo base_url(); ?>/assets/images/logo.png' alt='logo' class='img-fluid'></div>
				</div>
			</div>
			<div class='select-bid-prcent-section'>
				<div class='row'>
					<div class='col-lg-12'>
						<div class='bid_perc'>Select Your Bid Percentage </div>
						<div class='bitpercentage-box'><span class='bit-per'>0%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>25%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>50%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>75%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>100%</span></div>
					</div>
				</div>			
			</div>
		</div>
	</div>