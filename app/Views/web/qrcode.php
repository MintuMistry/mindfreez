<style>
#qrcode {
  width:160px;
  height:160px;
  margin-top:15px;
}
</style>
<div id="fullbox">
<div class="qr-code-img">
	<div class="container">
		<div class="row vertical-100">
			<div class="col-lg-12 align-self-center">
				<a href="<?php echo base_url();?>/admin/home/<?=$gameid?>">
					<div class="welcome">Welcome to MindFreeze</div>
					<div class="qr-scan" id="qrcode" ></div>
					<div class="quizid">Quiz ID : <?=$game_data[0]['quizid']?></div>
				</a>
			</div>
		</div>
	</div>
</div>
</div>
<!--<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>-->
<script src="<?php echo base_url(); ?>/assets/js/qrcode.min.js"></script>

<script type="text/javascript">
var qrcode = new QRCode("qrcode");
window.addEventListener('load', (event) => {
///function makeCode () {    
  //var elText = document.getElementById("text");
  var elText = '<?php echo base_url();?>/admin/home/<?php echo $gameid."@0";?>';
  
  if (!elText) {
    alert("Input a text");
    elText.focus();
    return;
  }
  
  qrcode.makeCode(elText);
//}
});
</script>