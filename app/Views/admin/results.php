﻿<style>
 /* Dropdown Button */
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 10px;
  font-size: 12px;
  border: none;
  cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
  background-color: #3e8e41;
}

/* The search field */
#myInput {
  box-sizing: border-box;
  background-image: url('searchicon.png');
  background-position: 14px 12px;
  background-repeat: no-repeat;
  font-size: 12px;
  padding: 14px 20px 12px 45px;
  border: none;
  border-bottom: 1px solid #ddd;
}

/* The search field when it gets focus/clicked on */
#myInput:focus {outline: 3px solid #ddd;}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: block;
  position: absolute;
  background-color: #f6f6f6;
  min-width: 150px;
  border: 1px solid #ddd;
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;} 
</style>
<main class="content">
<div class="container-fluid p-0">
	<div class="row justify-content-md-end pb-3 bordrstyle">
			<div class="form-group col-lg-4">
			<?php 
				if(!empty($error_msg))
				{
					echo '<div class="alert alert-danger" role="alert">'.$error_msg.'</div>';
				} 	
				//game_list
			?>
			<form action="<?php echo base_url('/admin/results'); ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
				   <select class="custom-select select-height" id="search_game" name="game_name">
				   <?php
						foreach($game_list as $game){?>
						<option value="<?=$game['game_name']?>"><?=$game['game_name']?></option>
						<?php } ?>
					</select>
					<span class="input-group-append">
						  <button type="submit" class="btn btn-info" >Go!</button>
						</span>
				</div>
				<div class="form-group col-lg-1">
					
						</div>
					</form>
			</div>
			<div class="form-group col-lg-4">
				<div class="form-group">
				   <select class="custom-select select-height" id="filter_result" onchange="changebox();">
					  <option selected="" value="1">Results by Team</option>
					  <option value="2">Results by Attendee</option>
					  <option value="3">Results by Question</option>
					</select>
				</div>
			</div>
		</div>
	<div id="resultbyteam" style="display:block;">
	<div class="tabclr">
	<div class="row">
		<div class="col-lg-6">Team name</div>
		<div class="col-lg-3">Total Points</div>
		<div class="col-lg-2">Rank</div>
		<div class="col-lg-1"></div>
	</div>
</div>
	<div id="payment_box p-0">
		
			<div class="accordion" id="accordionExample" >
			<?php
			//echo "<pre>";
			//print_r($results);
			//die();
			/* [teamresult_id] => 1
            [gameid] => 2
            [teamid] => 3
            [tscore] => 280
            [tpoints] => 280
            [team_name] => Team 1
            [rank] => 1
            [user_result] => Array
                (
                    [0] => Array
                        (
                            [result_id] => 1
                            [gameid] => 2
                            [teamid] => 3
                            [userid] => 1
                            [answer_string] => 100
                            [total_score] => 130
                            [total_points] => 130
                            [total_correct_answer] => 2
                            [full_name] => Test1
                        )
*/
			foreach($team_result as $result){
				
			?>
			  <div class="card">
				<div class="card-header tab-bodr" id="headingOne">
					<div class="row justify-content-center">
						<div class="col-lg-6 align-self-center"> <?=$result['team_name']?> </div>
						<div class="col-lg-3 align-self-center"><?=$result['tpoints']?></div>
						<div class="col-lg-2 align-self-center"><?=$result['rank']?></div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
				</div>

				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				  <div class="cardstyle">
				   <div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th scope="col"> Attendees Name </th>
								<th class="text-center" style=""> Total Score of each user </th>
								<th class="text-center" style=""> Total Correct Answers </th>
							</tr>
						</thead>
						<tbody>
						<?php 
						foreach($result['user_result'] as $user_result){?>
							<tr>
								<th scope="row"><?=$user_result['full_name']?> </th>
								<td class="text-center"><?=$user_result['total_points']?></td>
								<td class="text-center"><?=$user_result['total_correct_answer']?>/100</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				  </div>
				</div>
			  </div>
			  <?php } ?>
			</div>
		</div>
	</div><!--- result by Team block closed--->
	
<div id="resultbyattendee" style="display:none;">
	<div class="tabclr">
		<div class="row">
			<div class="col-lg-12">Attendee name</div>
		</div>
	</div>
	<div id="payment_box p-0">
			<div class="accordion" id="accordionExample">
						<?php
			//echo "<pre>";
			//print_r($attendee_result);
			//die();
			/*Array
(
    [0] => Array
        (
            [0] => Array
                (
                    [answer_id] => 1
                    [gameid] => 2
                    [userid] => 1
                    [quesid] => 2
                    [score] => 50
                    [points] => 50
                    [time_duration] => 20
                    [answer_status] => 1
                    [answer_given] => 14
                    [teamid] => 3
                    [full_name] => Test1
                    [team_name] => Team 1
                    [option_text] => Option2
                )

*/
			foreach($attendee_result as $attendee_data){?>
			  <div class="card">
				<div class="card-header tab-bodr" id="headingOne">
					<div class="row justify-content-center">
						<div class="col-lg-11 align-self-center"> <?=$attendee_data[0]['full_name'] ?> </div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
				</div>
				
				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				  <div class="cardstyle">
				   <div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th class="number-table"> Questions No. </th>
								<th> Answer Given - A/B/C/D </th>
								<th> Time </th>
								<th class="text-center"> Points </th>
								<th class="text-center"> Wrong/Right Answer </th>
								<th> Team </th>
							</tr>
						</thead>
						<tbody>
						<?php 
						$i =0;
							foreach($attendee_data as $aresult){
								$i++;
								?>
							<tr>
								<th><?=$i ?></th>
								<th scope="row"><?=$aresult['option_text'] ?> </th>
								<td><?=$aresult['time_duration'] ?></td>
								<td class="text-center"><?=$aresult['points'] ?></td>
								<td class="text-center">
								<?php 
								if($aresult['answer_status'] ==1){?>
								<i class="fa fa-check"></i>
								<?php }else{?>
								<i class="fa fa-times"></i>
								<?php }?>
								</td>
								<td><?=$aresult['team_name'] ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				  </div>
				</div>
			  </div>
			<?php } ?>
			</div>
			</div>
			</div><!--- result by Attendee block closed--->
			
	<div id="resultbyquestion" style="display:none;">	
<?php
			//echo "<pre>";
			//print_r($attendee_result);
			//die();
			/*Array
(
    [0] => Array
        (
            [0] => Array
                (
                    [answer_id] => 1
                    [gameid] => 2
                    [userid] => 1
                    [quesid] => 2
                    [score] => 50
                    [points] => 50
                    [time_duration] => 20
                    [answer_status] => 1
                    [answer_given] => 14
                    [teamid] => 3
                    [full_name] => Test1
                    [team_name] => Team 1
                    [option_text] => Option2
                )

*/			$i=0;
			foreach($question_result as $question_data){
				$i++;
				?>	
	<div class="tabclr">
		<div class="row">
			<div class="col-lg-12">Questions <?=$i ?></div>
		</div>
	</div>
	<div id="payment_box p-0">
		
			<div class="accordion" id="accordionExample">
			  <div class="card">
				<div class="card-header tab-bodr" id="headingOne">
					<div class="row justify-content-center">
						<div class="col-lg-11 align-self-center"><?=$i ?>. <?=$question_data[0]['team_name']?> </div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
				</div>

				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				  <div class="cardstyle">
				   <div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th> Attendees Name </th>
								<th> Answer Given - A/B/C/D </th>
								<th> Time </th>
								<th class="text-center"> Points </th>
								<th class="text-center"> Wrong/Right Answer </th>
								<th> Team </th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach($question_data as $qresult){?>
							<tr>
								<th><?=$qresult['full_name'] ?></th>
								<th scope="row"><?=$qresult['option_text'] ?> </th>
								<td><?=$qresult['time_duration'] ?></td>
								<td class="text-center"><?=$qresult['points'] ?></td>
								<td class="text-center">
								<?php 
								if($qresult['answer_status'] ==1){?>
								<i class="fa fa-check"></i>
								<?php }else{?>
								<i class="fa fa-times"></i>
								<?php }?>
								</td>
								<td><?=$qresult['team_name'] ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				  </div>
				</div>
			  </div>
				</div>
			</div>
			<?php } ?>
			
			</div><!--- result by question block close---->
		</div>
	</main>
	<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
<script>
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */

/* function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
} */ 

window.addEventListener('load', (event) => {
	document.getElementById("changebox").style.display = "block";	
	console.log('page is fully loaded');
});
/*  $(document).ready(function(){
	 $("#result_management").addClass("active");
      $('select').selectize({
          sortField: 'text'
  });
 }) */
function changebox(){
	var option_val = document.getElementById("filter_result").value;
	//alert(option_val);
	//return false;
	if(option_val == 1)
	{
		document.getElementById("resultbyteam").style.display = "block"; 
		document.getElementById("resultbyattendee").style.display = "none"; 
		document.getElementById("resultbyquestion").style.display = "none"; 
		
	}else if(option_val == 2)
	{
		document.getElementById("resultbyteam").style.display = "none"; 
		document.getElementById("resultbyattendee").style.display = "block"; 
		document.getElementById("resultbyquestion").style.display = "none";
		
	}else if(option_val == 3)
	{
		document.getElementById("resultbyteam").style.display = "none"; 
		document.getElementById("resultbyattendee").style.display = "none"; 
		document.getElementById("resultbyquestion").style.display = "block";
	}
	
}
</script>
</body>

</html>