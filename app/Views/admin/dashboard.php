<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap-clockpicker.min.css">
<main class="content game-show">
	<div class="container-fluid p-0">
		<div class="row mb-3">
			<div class="col-12 col-lg-12">
				<div class="rowbtn text-right mobcenter">
					<div class="btn import_btn" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Create Game </div>
				</div>
						<!--------------- Modal ---------------->
				<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title">Create Game</h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body">
								<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>admin/AddGameShow" enctype="multipart/form-data">
								<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-6">
									  <label for=""> Game Name </label>
									  <input type="text" class="form-control" id="game_name" name="game_name" placeholder=" Add your game name">
									</div>
									<div class="form-group col-md-6 game-location-icon">
									  <label for="">Game Location</label>
									  <input type="text" class="form-control" id="game_location" name="game_location" placeholder="Add game location">
									</div>
								  </div>
								  
								   <div class="form-row">
										<div class="form-group col-md-6 game-day-icon">
											<label for="inputAddress">Game Day</label>
											<input class="form-control" id="date" name="game_date" placeholder="MM/DD/YYY" type="text"/>
											
											<!--<div class='col-sm-3 input-group date' style="padding-left: 298px;" id='dtpickerdemo'>
											<input type='text' class="col-xs-10 col-sm-5" style="width: 346px;" name="event_start_date" value="" required=""/>
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>-->
										</div>
										<div class="form-group col-md-6 game-day-icon clockpicker" data-placement="left" data-align="top" data-autoclose="true">
										<label for="inputAddress">Game Time</label>
										
										<input type="text" class="form-control" name="game_time">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
									</div>
									

								  <div class="form-group text-center">
									 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
								  </div>
							</div>
							</form>
							</div>
							
						</div>
					</div>
				</div>
				<!------------- End modal------------- -->
			</div>
		  </div>
		<div class="row">
		<?php
		//echo "<pre>";
		//print_r($game_list);
		//die();
		$color_arr =array("firstcard","seccard","thirdcard","forthcard","fivecard","sixcard","sevencard","eightcard");
		$i = 0;
		foreach($game_list as $games){
		?>
			<div class="col-sm-3">
				<div class="card <?=$color_arr[$i]?>">
					<a href="<?php base_url(); ?>/admin/add_question/<?=$games['game_id']?>">
						<div class="card-body py-4">
							<div class="media">
								<div class="media-body">
									<h6 class="gameplace"><?=$games['game_location']?></h6>
									<h3 class="mb-2 h1 game_name text-center"> <?=$games['game_name']?> </h3>
									<div class="mb-0 h5 text-right game-time"> Time : <?=$games['game_time']?> </div>
									<div class="mb-0 h5 text-right game-time"> Date : <?=$games['game_date']?> </div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		<?php 
		
		if(count($color_arr)- 1 == $i)
		{
			$i = 0;
		}else{
			$i++; 
		}
		} ?>
		</div>									
	</div>
	</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap-clockpicker.min.js"></script>
<script>
 $(document).ready(function(){
	 $("#game_show_management").addClass("active");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active"); 
	
      var date_input=$('input[name="game_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
	  $('.clockpicker').clockpicker();
    })
	</script>
</body>

</html>