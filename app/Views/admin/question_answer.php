<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/question_answer_management">Question Answer Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					Question Answer List
				</h1>
				<div class="text-right">
		     		<div class="btn btn-info" data-toggle="modal" data-target="#add_question_answer" style="margin-top: -30px;">
		     			Add Question Answer
		     		</div>
		     	</div>
			</div>
<!----------------------  Modal for Add Question Answer -------------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_question_answer" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Question Answer</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>addQuestion_answer">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Question * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Question" class="col-xs-10 col-sm-5" name="question_text" required="" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> URL * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="URL" class="col-xs-10 col-sm-5" name="url" required="" />
											</div>
										</div>

										<div class="space-4"></div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Answer</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-2" placeholder="Answer" class="col-xs-10 col-sm-5" name="answer" required="" />
												
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

<!------------------------  Modal for Edit Question Answer ------------------------>
			<?php
				foreach($question_answers as $question_answer){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_question_answer<?=$question_answer['question_answer_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Question Answer</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>editQuestion_answer/<?=$question_answer['question_answer_id']?>">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Question * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Question" class="col-xs-10 col-sm-5" name="question_text" required="" value="<?= $question_answer['question_text']?>" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> URL * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="URL" class="col-xs-10 col-sm-5" name="url" required="" value="<?= $question_answer['url']?>"/>
											</div>
										</div>

										<div class="space-4"></div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Answer</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-2" placeholder="Answer" class="col-xs-10 col-sm-5" name="answer" required="" value="<?= $question_answer['answer']?>"/>
												
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
<!---------------------------- Question Answer List ---------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Question</th>
								<th scope="col">URL</th>
								<th scope="col">Answer</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							//print_r($user_details);
							foreach($question_answers as $question_answer){ 

								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $question_answer['question_text']?></td>
								<td><?= $question_answer['url']?></td>
								<td><?= $question_answer['answer']?></td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_question_answer<?=$question_answer['question_answer_id']?>"></i>
									</span>
									<a href="<?=base_url('admin')?>/deleteQuestion_answer/<?=$question_answer['question_answer_id']?>" class="tooltip-error" data-rel="tooltip" title="Delete">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
