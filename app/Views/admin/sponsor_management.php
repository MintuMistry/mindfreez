<main class="content game-show">
	<div class="container-fluid p-0">
		<div class="row mb-3">
			<div class="col-12 col-lg-12">
				<div class="rowbtn text-right mobcenter">
					<div class="btn import_btn" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add Sponsor</div>
				</div>
				<!---------------Add Modal ---------------->
		<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">Add Sponsor</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>AddSponsor" enctype="multipart/form-data">
							<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-12">
									  <label for=""> Sponsor Name </label>
									  <input type="text" class="form-control" id="sponsor_name" name="sponsor_name" placeholder="Add sponsor name">
									</div>
									<div class="col-md-6">
										<div class="box">
											<div class="js--image-preview"></div>
											<div class="upload-options">
											  <label>
												<input type="file" class="image-upload" name="images" accept="image/*" />
											  </label>
											</div>
										  </div>
									</div>
								  </div>
								  <div class="form-group text-center">
									 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
								  </div>
							</div>
						</form>
					</div>
					
				</div>
			</div>
		</div>
		<!------------- End modal------------- -->
			</div>
		  </div>
		<div class="row">
		<?php 
			//echo "<pre>";
			//print_r($sponsor_list);
			//die();
			foreach($sponsor_list as $sponsor)
			{?>
			
		<!--------------- Edit Modal ---------------->
		<div class="modal fade show" id="EditSponsor<?=$sponsor['sponsor_id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">Edit Sponsor</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>EditSponsor/<?=$sponsor['sponsor_id']?>" enctype="multipart/form-data">
							<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-12">
									  <label for=""> Sponsor Name </label>
									  <input type="text" class="form-control" id="sponsor_name" name="sponsor_name" placeholder="Edit sponsor name" value="<?= $sponsor['sponsor_name']?>">
									</div>
									<div class="col-md-6">
										<div class="box">
											<div class="js--image-preview">
											<img src="<?php echo base_url()."/writable/uploads/".$sponsor['sponsor_img']?>" style="height: 200px; width: 257px;" />
											</div>
											<div class="upload-options">
											  <label>
												<input type="file" class="image-upload" name="images" accept="image/*" />
											  </label>
											</div>
										  </div>
									</div>
								  </div>
								  <div class="form-group text-center">
									 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
								  </div>
							</div>
						</form>
					</div>
					
				</div>
			</div>
		</div>
		
		
		<!--------------- View Modal ---------------->
		<div class="modal fade show" id="ViewSponsor<?=$sponsor['sponsor_id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">View Sponsor</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
					<div class="table-responsive  border">
					<table class="table mb-0">
						<thead>
							<tr>
								<th scope="col">Sponsor Image </th>
								<th scope="col">Sponsor Name </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row"><img src="<?php echo base_url()."/writable/uploads/".$sponsor['sponsor_img']?>" style="height: 100px; width: 1oopx;" /></th>
								<td><?=$sponsor['sponsor_name'] ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
				</div>
			</div>
		</div>
		
		<?php 
			}
			
			foreach($sponsor_list as $sponsor)
			{?>
			<div class="col-sm-3">
				<div class="card firstcard heigh157">
					<div class="card-body py-4" style="background-image: url(<?php echo base_url()."/writable/uploads/".$sponsor['sponsor_img']?>);">
						<div class="media">
							<div class="media-body">
								<h6 class="iconplace">
									<a href="<?php base_url(); ?>DeleteSponsor/<?=$sponsor['sponsor_id']?>"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
									
									<i class="align-middle mr-2 fas fa-pencil-alt" data-toggle="modal" data-target="#EditSponsor<?=$sponsor['sponsor_id']?>"></i>
									
									<i class="align-middle fas fa-eye" data-toggle="modal" data-target="#ViewSponsor<?=$sponsor['sponsor_id']?>" style="color:#fff;"></i>
									
								</h6>
								<h3 class="mb-2 mt-4 h1 sponser_name text-center"><?=$sponsor['sponsor_name'] ?> </h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
		
		?>									
	</div>
	</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/upload-img.js"></script>
<script>
	$(document).ready(function (){
	
	$("#sponsor_management").addClass("active");
	
	})
</script>
</body>

</html>