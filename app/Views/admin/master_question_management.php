﻿<style>
.fa-disabled {
  opacity: 0.6;
  cursor: not-allowed;
}
</style>
<main class="content">
<div class="container-fluid p-0">
<div class="row mb-2">
	<div class="col-12 col-lg-6 mobcenter">
		<h3 class="web-clr"> Master Question Managemnt </h3>
	</div>
	<div class="col-12 col-lg-6">
		<div class="rowbtn text-right mobcenter">
			<div class="btn import_btn" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i>  Add Questions </div>
		</div>
<!--------------- Modal ---------------->
		<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">  Add Questions </h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
				<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>AddQuestion" enctype="multipart/form-data">
					<div id="formbox">
						  <div class="form-row">
							<div class="form-group col-md-12">
							  <label for=""> Add Question </label>
							  <div class="input-group-addon">?</div>
							  <input type="text" class="form-control input-icon" id="question" name="question" placeholder=" Add your question">
							</div>
						  </div>

						  <div class="row">
							<div class="col-lg-12 mb-2 mt-2"> Add Options </div>
							<div class="col-lg-12">
							  <div class="row" id="target">
								<div class="col-lg-12">
								  <div class="input-group option_class" id="duplicater">
									<input class="form-control" type="text" name="option_text[]">
									<span class="input-group-btn">
									<input type="button" class="btn removeBtn"><i id= "remove_Btn" class="fa fa-times fa-disabled" onclick="remove(this)"></i>
									</span>
									<div class="leabelbox">
										<label class="checkboxuse correct-question">
											  <input type="checkbox" id="checked_btn" name="correct_answer" onclick="manageClick(this)" value="0">
											  <span class="checkmark"></span>
										</label>
									</div>
								  </div>
								</div>
							  </div>
							  <div class="row">
								<div class="col-lg-12 text-right">
								  <input type="button" class="btn add_opt" id="option_button" onclick="duplicate()" value="Add Options"/>
								</div>
							  </div>
							</div>
						  </div>
						  
						  <div class="row">
						  <div class="col-lg-12"> 
								<div class="form-group">
									<label>Explanation of correct answer</label>
									<textarea type="text" class="form-control" id="explanation" name="explanation" placeholder="Explanation of correct answer"></textarea>
								  </div>
							</div>
							</div>
							
						  <div class="row">
							<div class="col-lg-3"> 
								<div class="form-group">
									<label>Max. Time limit</label>
									<input type="number" class="form-control" id="time_limit" name="time_limit" >
								  </div>
							</div>
							
							<div class="col-lg-9">
								<label>Add Video/Image</label>
								<div class="custom-file">
								  <input type="file" class="custom-file-input" id="customFile" name="customFile">
								  <label class="custom-file-label upload-height" for="customFile">Choose file</label>
								</div>
								<div class="uploaded-video-img pt-4"><img src="<?php echo base_url(); ?>/assets/images/uploaded-img.png"></div>
							</div>
						  </div>
						  <div class="form-group text-center mt-3">
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn saveclose"> Save and Close </button>
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn"> Save and New </button>
						  </div>
					</div>
				</form>
				</div>
				</div>
			</div>
		</div>

	</div>
  </div>
<div class="tabclr">
<div class="row">
	<div class="col-lg-1">No.</div>
	<div class="col-lg-3">Video/Image</div>
	<div class="col-lg-5">Questions</div>
	<div class="col-lg-1">Time</div>
	<div class="col-lg-2 text-center acti-pad">Action</div>
</div>
</div>
<div id="payment_box">
	<div class="row justify-content-md-end pb-3 bordrstyle">
		<div class="form-group col-lg-4 align-self-end mt-2">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-append">
					  <button class="btn btn-info" type="button">Go!</button>
					</span>
				</div>
			</div>
	</div>
		<div class="accordion" id="accordionExample">
		  
		  <?php
		  /*  [2] => Array
        (
            [question_id] => 4
            [question_text] => Testing Question3
            [time_limit] => 30
            [filepath] => 20201113/1605246574_462ce10cf465a023acab.jpg
            [options] => Array
                (
                    [0] => Array
                        (
                            [option_id] => 2
                            [question_id] => 4
                            [option_text] => Option1
                        )

                    [1] => Array
                        (
                            [option_id] => 3
                            [question_id] => 4
                            [option_text] => Option2
                        )

                    [2] => Array
                        (
                            [option_id] => 4
                            [question_id] => 4
                            [option_text] => Option3
                        )

                    [3] => Array
                        (
                            [option_id] => 5
                            [question_id] => 4
                            [option_text] => Option4
                        )

                    [4] => Array
                        (
                            [option_id] => 6
                            [question_id] => 4
                            [option_text] => Option5
                        )

                ) */
		  $i=0;
			foreach($question_list as $questions)
			{
				$i++;
		  ?>
		  
		  <!--------------- Modal ---------------->
		<div class="modal fade show" id="EditQuestion<?=$questions['question_id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">  Edit Questions </h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
				<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>EditQuestion/<?=$questions['question_id']?>" enctype="multipart/form-data">
					<div id="formbox">
						  <div class="form-row">
							<div class="form-group col-md-12">
							  <label for=""> Edit Question </label>
							  <div class="input-group-addon">?</div>
							  <input type="text" class="form-control input-icon" id="question" name="question" value="<?=$questions['question_text']?>">
							</div>
						  </div>
						  
						  <div class="row">
							<div class="col-lg-12 mb-2 mt-2"> Add Options </div>
							<div class="col-lg-12">
							  <div class="row" id="target">
								<div class="col-lg-12">
								<?php
								if(count($questions['options']) > 0)
								{
									$option_arr  = $questions['options'];
									$m='';
									foreach($option_arr as $options)
									{?>
									
									<div class="input-group option_class_new" id="duplicater_new<?=$m?>">
									<input class="form-control" type="text" name="option_text[]" value="<?=$options['option_text']?>">
									<span class="input-group-btn">
									<input type="button" class="btn removeBtn"><i id= "remove_Btn_new<?=$m?>" class="fa fa-times" onclick="remove_new(this)"></i>
									</span>
									<div class="leabelbox">
										<label class="checkboxuse correct-question">
										<?php 
										if($questions['correct_answer'] == $options['option_id']){?>
										<input type="checkbox" id="checked_btn_new" name="correct_answer" onclick="manageClick_new(this)" value="<?=$m?>" checked >
											  <span class="checkmark"></span>
										<?php }else{?>
										 <input type="checkbox" id="checked_btn_new<?=$m?>" name="correct_answer" onclick="manageClick_new(this)" value="<?=$m?>">
											  <span class="checkmark"></span>
										<?php }?>
										</label>
									</div>
								  </div>
								  <?php	
								  if($m =='')
								  {
									  $m =1;
								  }else{
									  $m++;
								  }
								  
									}
								}
								?>
								</div>
							  </div>
							  <div class="row">
								<div class="col-lg-12 text-right">
								  <input type="button" class="btn add_opt duplicate_new" id="option_button_new" value="Add Options"/>
								</div>
							  </div>
							</div>
						  </div>
						  
						  <div class="row">
						  <div class="col-lg-12"> 
								<div class="form-group">
									<label>Explanation of correct answer</label>
									<textarea type="text" class="form-control" id="explanation" name="explanation" placeholder="Explanation of correct answer"><?=$questions['explanation']?></textarea>
								  </div>
							</div>
							</div>
							
							
						  <div class="row">
							<div class="col-lg-3"> 
								<div class="form-group">
									<label>Max. Time limit</label>
									<input type="number" class="form-control" id="time_limit" value="<?=$questions['time_limit']?>" name="time_limit" >
								  </div>
							</div>
							<div class="col-lg-9">
								<label>Add Video/Image</label>
								<div class="custom-file">
								  <input type="file" class="custom-file-input" id="customFile" name="customFile">
								  <label class="custom-file-label upload-height" for="customFile">Choose file</label>
								</div>
								<div class="uploaded-video-img pt-4"><img src="<?php echo base_url()."/writable/uploads/".$questions['filepath']?>" height="100px" width="100px"></div>
							</div>
						  </div>
						  <div class="form-group text-center mt-3">
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn saveclose"> Save and Close </button>
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn"> Save and New </button>
						  </div>
					</div>
				</form>
				</div>
				</div>
			</div>
		</div>
		
		  <div class="card">
			<div class="card-header tab-bodr" id="headingOne">
				<div class="row justify-content-center">
					<div class="col-lg-1 align-self-center text-center"> <?= $i?>. </div>
					<div class="col-lg-2 align-self-center"><div class="vid-img"><img src="<?php echo base_url()."/writable/uploads/".$questions['filepath']?>"></div> </div>
					<div class="col-lg-6 align-self-center"><div class="question"><?=$questions['question_text']?></div> </div>
					<div class="col-lg-1 align-self-center"><div class="quiestion-time"> <?=$questions['time_limit']?> Sec</div> </div>
					<div class="col-lg-2 align-self-center"><div class="action-pos">
						
						<i class="align-middle mr-2 fas fa-fw fa-edit" data-toggle="modal" data-target="#EditQuestion<?=$questions['question_id']?>"></i>
						
						
						<a href="<?php echo base_url()?>/admin/deleteQuestion/<?=$questions['question_id']?>"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
						<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<i class="fa fa-angle-down"></i>
							<i class="fa fa-angle-up"></i>
						</a>
					</div> 
					</div>
				</div>
			</div>
			

			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
			  <div class="card-body cardstyle">
				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			  </div>
			</div>
			 </div>
			<?php } ?>
		 
			</div>
		</div>
	</div>
</main>
<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/add_input.js"></script>
	
<script>

var i = 0;
var j = <?php echo $m-1 ?>;
var original = document.getElementById('duplicater');
var original1 = document.getElementById('duplicater_new0');

window.addEventListener('load', (event) => {
	var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i;
	//clone.find("#remove_Btn").attr("id","remove_Btn"+ ++i);
	clone.getElementsByTagName('i')[0].id = "remove_Btn" + i;
	clone.getElementsByTagName('input')[2].value = i;
    // or clone.id = ""; if the divs don't need an ID
    original.parentNode.appendChild(clone);
	document.getElementById('remove_Btn' + i).removeAttribute("onclick");
	console.log('page is fully loaded');
});

function duplicate() {
	
		var clone = original.cloneNode(true); // "deep" clone
		clone.id = "duplicater" + ++i;
		// or clone.id = ""; if the divs don't need an ID
		clone.getElementsByTagName('input')[0].value = "";
		clone.getElementsByTagName('i')[0].id = "remove_Btn" + i;
		clone.getElementsByTagName('input')[2].id = "checked_btn" + i;
		clone.getElementsByTagName('input')[2].checked = false;
		clone.getElementsByTagName('input')[2].value = i;
		//el.value = i;
		if(document.getElementsByClassName("option_class").length <= 7)
		{
			original.parentNode.appendChild(clone);
			//document.getElementById('remove_Btn').setAttribute('onclick','$(this).closest("div").remove()');
			//document.getElementById('buttonLED'+id).onclick = function(){ return writeLED(1,1)};
			var elem = document.getElementById( 'remove_Btn' + i);
			//elem.classList.add('fa-disabled'); // Add class
			elem.classList.remove('fa-disabled'); // Remove class
			document.getElementById("option_button").disabled = false;
		}else{
			document.getElementById("option_button").disabled = true;
		}
    
}

/* function duplicate1() {
	//alert("dfjhbd");
	//return false;
    var clone1 = original1.cloneNode(true); // "deep" clone
    clone1.id = "duplicater1" + ++i;
    // or clone.id = ""; if the divs don't need an ID
    original1.parentNode.appendChild(clone1);
} */

$(document).ready(function (){
	
	$("#question_management").addClass("active");
	//alert("It works");

/* $('.modal').find('button').click(function () {
    // Your current generate() function code
}); */
$('.duplicate_new').click(function (){
	//$('#duplicater_new0').append($('#duplicater_new0').html());
	//var $clonedContent = $('#duplicater_new0').clone();
	var ele = $(this).closest('.modal');
	alert(ele);
	return false;
	var clonedDiv = ele.find('#duplicater_new0').clone();
	clonedDiv.attr("id", "newId");
	$('#duplicater_new' + j).after('#duplicater_new' + j);

	//$clonedContent = ele.find('#duplicater_new0').clone();
	//var $newdivid = $clonedContent.prop('id', 'duplicater_new0'+ ++j );
	//$clonedContent.attr('id', 'duplicater_new'+ ++j).insertAfter("div#duplicater_new" + j +":last");
	
	//ele.find('#duplicater_new')
         // .clone()
          //.attr('id', 'duplicater_new'+ ++j)
		  //.insertAfter("div#duplicater_new" + j +":last");
          //.insertAfter('[id^=id]:last'); 
           //            ^-- Use '#id' if you want to insert the cloned 
           //                element in the beginning
          //.text('Cloned ' + (cloneCount-1)); //<--For DEMO

  // Manipulate the cloned element
  // -- Update the existing content
  //$clonedContent.find('h5').text("My content just got manipulated");
	//ele.find('#duplicater_new' + ++j).appendChild($clonedContent);
	//original.parentNode.appendChild(clone);
});

})
 function remove(el) {
	
	var currentchild = el.id;
	var num = currentchild.match(/\d+/)[0] // "3"
	//alert(num);
	var parentdivid = 'duplicater'+num;
	if(parentdivid == 'duplicater' || parentdivid == 'duplicater1')
	{
		document.getElementById(parentdivid).disabled = true;
		
	}else{
		document.getElementById(parentdivid).remove();
		document.getElementById("option_button").disabled = false;
		//element.remove();parentdivid
	}
}

    function getCheckboxes() {
        return document.querySelectorAll('input[type=checkbox]');
    }

    function uncheckAllCheckboxes() {
        var checkboxes = getCheckboxes();

        for (var i = 0, length = checkboxes.length; i < length; i++) {
            checkboxes[i].checked = false;
        }
    }

    function manageClick(el) {
        uncheckAllCheckboxes();
        //this.checked = true;
        el.checked = true;
		//el.value = i;
		//document.getElementById("option_button").disabled = false;
    }

    function init() {
        var checkboxes = getCheckboxes();

        for (var i = 0, length = checkboxes.length; i < length; i++) {
            checkboxes[i].addEventListener('click', manageClick);
        }
    }
	
	function remove_new(el) {
	
	var currentchild = el.id;
	var num = currentchild.match(/\d+/)[0] // "3"
	var parentdivid = 'duplicater_new'+num;
	//alert(parentdivid);
	//return false;
	if(parentdivid == 'duplicater_new' || parentdivid == 'duplicater_new1')
	{
		document.getElementById(parentdivid).disabled = true;
		
	}else{
		document.getElementById(parentdivid).remove();
		document.getElementById("option_button_new").disabled = false;
		//element.remove();parentdivid
	}
	}
	
	 function getCheckboxes_new() {
        return document.querySelectorAll('input[type=checkbox]');
    }

    function uncheckAllCheckboxes_new() {
        var checkboxes = getCheckboxes_new();

        for (var i = 0, length = checkboxes.length; i < length; i++) {
            checkboxes[i].checked = false;
        }
    }

    function manageClick_new(el) {
        uncheckAllCheckboxes_new();
        //this.checked = true;
        el.checked = true;
		//el.value = i;
		//document.getElementById("option_button").disabled = false;
    }

    function init_new() {
        var checkboxes = getCheckboxes_new();

        for (var i = 0, length = checkboxes.length; i < length; i++) {
            checkboxes[i].addEventListener('click', manageClick_new);
        }
    }
	

    //init();

</script>
</body>

</html>