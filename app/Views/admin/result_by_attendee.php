﻿<main class="content">
<div class="container-fluid p-0">
	<div class="row justify-content-md-end pb-3 bordrstyle">
			<div class="form-group col-lg-4">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search by Game">
						<span class="input-group-append">
						  <button class="btn btn-info" type="button">Go!</button>
						</span>
					</div>
			</div>
			<div class="form-group col-lg-4">
				<div class="form-group">
				   <select class="custom-select select-height">
					  <option >Results by Team</option>
					  <option selected=""value="1">Results by Attendee</option>
					  <option value="2">Results by Question</option>
					</select>
				</div>
			</div>
		</div>
	<div class="tabclr">
	<div class="row">
		<div class="col-lg-12">Attendee name</div>
	</div>
</div>
	<div id="payment_box p-0">
		
			<div class="accordion" id="accordionExample">
			  <div class="card">
				<div class="card-header tab-bodr" id="headingOne">
					<div class="row justify-content-center">
						<div class="col-lg-11 align-self-center"> Name of the Attendee dummy text </div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
				</div>

				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				  <div class="cardstyle">
				   <div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th class="number-table"> Questions No. </th>
								<th> Answer Given - A/B/C/D </th>
								<th> Time </th>
								<th class="text-center"> Points </th>
								<th class="text-center"> Wrong/Right Answer </th>
								<th> Team </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>1.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>2.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>3.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-times"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>4.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>5.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>

						</tbody>
					</table>
				</div>
				  </div>
				</div>
			  </div>
				  <div class="card">
					<div class="card-header tab-bodr" id="headingTwo">
						<div class="row justify-content-center">
						<div class="col-lg-6 align-self-center"> Team name dummy text </div>
						<div class="col-lg-3 align-self-center">70</div>
						<div class="col-lg-2 align-self-center">1st</div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					  <div class="cardstyle">
						<div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th class="number-table"> Questions No. </th>
								<th> Answer Given - A/B/C/D </th>
								<th> Time </th>
								<th class="text-center"> Points </th>
								<th class="text-center"> Wrong/Right Answer </th>
								<th> Team </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>1.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>2.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>3.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-times"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>4.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>5.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>

						</tbody>
					</table>
				</div>
					  </div>
					</div>
				  </div>
				  <div class="card">
					<div class="card-header tab-bodr" id="headingThree">
						 <div class="row justify-content-center">
						<div class="col-lg-6 align-self-center"> Team name dummy text </div>
						<div class="col-lg-3 align-self-center">70</div>
						<div class="col-lg-2 align-self-center">1st</div>
						<div class="col-lg-1 align-self-center text-right">
							<a class="dropicon" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up"></i>
							</a>
						</div>

					</div>
					   
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					  <div class="cardstyle">
						 <div class="table-responsive">
					<table class="table mb-0">
						<thead class="gray-thead">
							<tr>
								<th class="number-table"> Questions No. </th>
								<th> Answer Given - A/B/C/D </th>
								<th> Time </th>
								<th class="text-center"> Points </th>
								<th class="text-center"> Wrong/Right Answer </th>
								<th> Team </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>1.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>2.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>3.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-times"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>4.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>
							<tr>
								<th>5.</th>
								<th scope="row">Lorem ipsum dolor sit amet,adipiscing elit. </th>
								<td>00:20</td>
								<td class="text-center">50</td>
								<td class="text-center"><i class="fa fa-check"></i></td>
								<td>Lorem ipsum dolor.</td>
							</tr>

						</tbody>
					</table>
				</div>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</main>
<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>

</body>

</html>