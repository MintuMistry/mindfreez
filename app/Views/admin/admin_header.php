<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Mind Freeze </title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/classic.css">
</head>

<body>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
			<a class="sidebar-brand" href="<?php echo base_url(); ?>">
          		<span class="align-middle"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo" class="img-responsive"></span>
        	</a>

				<ul class="sidebar-nav">
					<li class="sidebar-item" id="game_show_management">
						<a href="<?php echo base_url(); ?>" class="sidebar-link">
			             <span class="align-middle">Game Show Management</span>
			            </a>
					</li>
					<li class="sidebar-item" id="sponsor_management">
						<a href="<?php echo base_url(); ?>/admin/sponsor_management" class="sidebar-link">
              				<span class="align-middle"> Sponsor Management </span>
            			</a>
					</li>
					<li class="sidebar-item" id="user_management">
						<a href="<?php echo base_url(); ?>/admin/master_user_management" class="sidebar-link">
              				<span class="align-middle"> Master User Management </span>
            			</a>
					</li>
					<li class="sidebar-item" id="question_management">
						<a href="<?php echo base_url(); ?>/admin/master_question_management" class="sidebar-link">
              				<span class="align-middle"> Master Question Management </span>
            			</a>
					</li>
					<li class="sidebar-item" id="attendee_management">
						<a href="<?php echo base_url(); ?>/admin/manage_attendees" class="sidebar-link">
              				<span class="align-middle"> Manage Attendees </span>
            			</a>
					</li>
					<li class="sidebar-item" id="result_management">
						<a href="<?php echo base_url(); ?>/admin/results" class="sidebar-link">
              				<span class="align-middle"> Results </span>
            			</a>
					</li>
					
					<li class="sidebar-item" id="website_management">
						<a href="<?php echo base_url(); ?>/admin/website_management" class="sidebar-link">
              				<span class="align-middle"> Website Management </span>
            			</a>
					</li>
				</ul>
				
			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light bg-white">
				<a class="sidebar-toggle d-flex mr-2">
		          <i class="hamburger align-self-center"></i>
		        </a>
				<div class="navbar-collapse collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
							<!--<img src="<?php echo base_url()."/writable/uploads/".$admin_data->photo ?>" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"> <span class="text-dark"><?=$admin_data->name ?> </span>-->
							
				                <img src="<?php echo base_url(); ?>/assets/images/avatars/avatar.jpg" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"> <span class="text-dark">Randolph A Robinson </span>
				              </a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="<?php echo base_url(); ?>/admin/profile"> Profile</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?php echo base_url(); ?>/admin/logout">Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>