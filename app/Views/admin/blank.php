﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Mind Freeze </title>
	<link rel="stylesheet" href="css/classic.css">
</head>

<body>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
			<a class="sidebar-brand" href="index.html">
          		<span class="align-middle"><img src="img/logo.png" alt="logo" class="img-responsive"></span>
        	</a>

				<ul class="sidebar-nav">
					<li class="sidebar-item active">
						<a href="index.html" class="sidebar-link">
			             <span class="align-middle">Game Show Management</span>
			            </a>
					</li>
					<li class="sidebar-item">
						<a href="sponsor_management.html" class="sidebar-link">
              				<span class="align-middle"> Sponsor Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="master_user_management.html" class="sidebar-link">
              				<span class="align-middle"> Master User Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="master_question_management.html" class="sidebar-link">
              				<span class="align-middle"> Master Question Management </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="manage_attendees.html" class="sidebar-link">
              				<span class="align-middle"> Manage Attendees </span>
            			</a>
					</li>
					<li class="sidebar-item">
						<a href="results.html" class="sidebar-link">
              				<span class="align-middle"> Results </span>
            			</a>
					</li>
				</ul>
			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light bg-white">
				<a class="sidebar-toggle d-flex mr-2">
		          <i class="hamburger align-self-center"></i>
		        </a>
				<div class="navbar-collapse collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
				                <img src="img\avatars\avatar.jpg" class="avatar img-fluid rounded-circle mr-1" alt="Chris Wood"> <span class="text-dark">Randolph A Robinson </span>
				              </a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="profile.html"> Profile</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>

			<main class="content game-show">
				
			</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
	<script src="js\app.js"></script>

</body>

</html>