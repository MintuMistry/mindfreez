﻿<main class="content">
<div class="container-fluid p-0">
		<div class="row mb-2">
		<div class="col-12 col-lg-6 mobcenter">
			<h3 class="web-clr"> User Management </h3>
		</div>
		<div class="col-12 col-lg-6">
			<div class="rowbtn text-right mobcenter">
				<div class="btn import_btn" data-toggle="modal" data-target="#sizedModalSm"><i class="align-middle mr-2 fas fa-fw fa-plus"></i> Add a new user </div>
			</div>
<!---------------Add  Modal ---------------->
			<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title"> Add a new user</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>AddUser">
								<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-6">
									  <label for=""> Full Name </label>
									  <input type="text" class="form-control" id="full_name" name="full_name" placeholder=" Add user name">
									</div>
									<div class="form-group col-md-6">
									  <label for="">Phone Number</label>
									  <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter your phone number">
									</div>
								  </div>
								   <div class="form-row">
									<div class="form-group col-md-6">
									  <label for="">Email</label>
									  <input type="text" class="form-control" id="email" name="email" placeholder="Enter your email address">
									</div>
									<div class="form-group col-md-6">
									  <label for="">Password</label>
									  <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
									</div>
								  </div>
								   <div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputAddress">Street Address</label>
											<input type="text" class="form-control" id="street_address" name="street_address" placeholder="1234 Main St">
										</div>
										<div class="form-group col-md-6">
											<label for="inputAddress2">City</label>
											<input type="text" class="form-control" id="city" name="city" placeholder="City Name">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputCity">State</label>
											<input type="text" class="form-control" id="state" name="state" placeholder="New York">
										</div>
										<div class="form-group col-md-6">
											<label for="inputState">Zip</label>
											<input type="text" class="form-control" id="zip" name="zip" placeholder="Enter Zip code">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputZip">Country</label>
											<input type="text" class="form-control" id="country" name="country" placeholder="United States">
										</div>
									</div>

								  <div class="form-group text-center">
									 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
								  </div>
							</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>

		</div>
	  </div>

	  <div class="row">
		  <div class="col-12 col-lg-12">
			<div class="card">
				<div class="row">
					<div class="col-12 col-lg-12 mt-minus">
					<div class="table-responsive  border">
					<table class="table mb-0">
						<thead>
							<tr>
								<th scope="col">Sr No. </th>
								<th scope="col" style="">Full Name</th>
								<th scope="col" style="">User Email</th>
								<th scope="col" style="">User Phone</th>
								<th scope="col">Address</th>
								<th scope="col">Zip Code</th>
								<th scope="col">City</th>
								<th scope="col">State</th>
								<th scope="col">Country</th>
								<th scope="col">Status</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						
						
						//echo "<pre>";
						//print_r($user_details);
						//die();
						/* [user_id] => 2
            [full_name] => Test2
            [phone_number] => 9456789876
            [email] => abc@gmail.com
            [password] => 123
            [street_address] => 123 Main St
            [city] => Faridabad
            [zip] => 234567
            [country] => India
			[is_active] => 1
            [state] => Haryana */
			$i=0;
			foreach($user_details as $user){
				$i++;
						?>
							<tr>
								<th scope="row"><?=$i ?></th>
								<td><?=$user['full_name']?></td>
								<td><?=$user['email']?></td>
								<td><?=$user['phone_number']?></td>
								<td><?=$user['street_address']?></td>
								<td><?=$user['zip']?></td>
								<td><?=$user['city']?></td>
								<td><?=$user['state']?></td>
								<td><?=$user['country']?></td>
								
								
								<td class="view">
								<?php 
								
								if($user['is_active'] ==1){?>
								<span class="badge badge-success">Active</span> 
								<?php }else{?>
								<span class="badge badge-warning">Inactive</span> 
								<?php }?>
								</td>
								<td>
								<i class="align-middle mr-2 fas fa-fw fa-edit" data-toggle="modal" data-target="#EditUser<?=$user['user_id']?>"></i>
								
								<a href="<?php base_url(); ?>deleteUser/<?=$user['user_id']?>"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
								
								<!--<i class="align-middle fas fa-eye" data-toggle="modal" data-target="#ViewUser<?=$user['user_id']?>" style="color:#fff;"></i>-->

								</td>
							</tr>
					<?php } ?>
						</tbody>
					</table>
			<?php
			foreach($user_details as $user){
			?>
			<!--------------- Modal ---------------->
			<div class="modal fade show" id="EditUser<?=$user['user_id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title"> Edit user</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span>
							</button>
						</div>
						
						<div class="modal-body">
						<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>editUser/<?=$user['user_id']?>">
								<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-6">
									  <label for=""> Full Name </label>
									  <input type="text" class="form-control" id="full_name" name="full_name" value="<?= $user['full_name']?>" placeholder=" Edit user name">
									</div>
									<div class="form-group col-md-6">
									  <label for="">Phone Number</label>
									  <input type="text" class="form-control" id="phone_number" value="<?= $user['phone_number']?>" name="phone_number" placeholder="Enter your phone number">
									</div>
								  </div>
								   <div class="form-row">
									<div class="form-group col-md-6">
									  <label for="">Email</label>
									  <input type="text" class="form-control" id="email" name="email" value="<?= $user['email']?>" placeholder="Enter your email address">
									</div>
									
										<div class="form-group col-md-6">
											<label for="inputAddress">Street Address</label>
											<input type="text" class="form-control" value="<?=$user['street_address']?>" id="street_address" name="street_address" placeholder="1234 Main St">
										</div>
										</div>
										<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputAddress2">City</label>
											<input type="text" class="form-control" value="<?=$user['city']?>" id="city" name="city" placeholder="City Name">
										</div>
									
									
										<div class="form-group col-md-6">
											<label for="inputCity">State</label>
											<input type="text" class="form-control" value="<?=$user['state']?>" id="state" name="state" placeholder="New York">
										</div>
										</div>
										<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputState">Zip</label>
											<input type="text" class="form-control" value="<?=$user['zip']?>" id="zip" name="zip" placeholder="Enter Zip code">
										</div>
									
										<div class="form-group col-md-6">
											<label for="inputZip">Country</label>
											<input type="text" class="form-control" value="<?=$user['country']?>" id="country" name="country" placeholder="United States">
										</div>
									</div>
									<div class="form-row">
									
									<div class="form-group col-md-6">
											<label for="inputStatus">Status</label>
											<select class="form-control" id="status" name="status">
											<?php
											if($user['is_active'] == 1)
											{?>
											<option style="color:black;" value="<?=$user['is_active']?>" selected>Active</option>
											<?php }else{?>
											
											<option style="color:black;" value="1" >Active</option>
											<?php }?>
											
											<?php if($user['is_active'] == 0){?>
											<option style="color:black;" value="<?=$user['is_active']?>" selected>Inactive</option>
											<?php }else{?>
											<option style="color:black;" value="0" >Inactive</option>
											
											
											<?php }?>
											
											</select>
										</div>
										</div>
								<div class="form-row">
								  <div class="form-group text-center">
									 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Submit </button>
								  </div>
							</div>
							</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
			
			<!--------------- Modal ---------------->
			<div class="modal fade show" id="user_mng_view" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
					<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header webclr">
							<h4 class="modal-title clrwhite heading" id="">User Details</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span>
							</button>
						  </div>
						<div class="modal-body">
							<form>
								<div id="formbox">
								  <div class="form-row">
									<div class="form-group col-md-6">
									  <label for=""> User Name </label>
									  <input type="text" class="form-control" id="" placeholder="Chris Wood">
									</div>
									 <div class="form-group col-md-6">
									  <label for="">Phone Number</label>
									  <input type="password" class="form-control" id="" placeholder="+1202-555-0123">
									</div>
									
								  </div>
								   <div class="form-row">
									<div class="form-group col-md-6">
									  <label for="">Email Address</label>
									  <input type="text" class="form-control" id="" placeholder="example@gmail.com">
									</div>
								  </div>
								   <div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputAddress">Street Address</label>
											<input type="text" class="form-control" id="" placeholder="1234 Main St">
										</div>
										<div class="form-group col-md-6">
											<label for="inputAddress2">City</label>
											<input type="text" class="form-control" id="" placeholder="Apartment, studio, or floor">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputCity">State</label>
											<input type="text" class="form-control" id="" placeholder="New York">
										</div>
										<div class="form-group col-md-6">
											<label for="inputState">Zip</label>
											<input type="text" class="form-control" id="" placeholder="Enter Zip code">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="inputZip">Country</label>
											<input type="text" class="form-control" id="" placeholder="United States">
										</div>
									</div>

							</div>
							</form>
						</div>
						
					</div>
				</div>
				</div>
				<?php } ?>
			</div>
<!-------- End Modal -------->
			</div>
		</div>
	</div>
	</div>
</div>
</div>
</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>

	<script>
	$(document).ready(function (){
	
	$("#user_management").addClass("active");
	
	})
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>