﻿<main class="content">
<div class="container-fluid p-0">
	<div class="row">
		<div class="col-lg-9">
			<div class="team_all">
				<div class="row">
				<?php
					foreach($teams_details as $team){
						//echo "<pre>";
						//print_r($team);
						
						?>
			<!--------------- Modal ---------------->
			<div class="modal fade" id="EditTeam<?=$team['team_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog  modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title">Edit Team</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>EditTeam/<?=$team['team_id']?>" enctype="multipart/form-data">
								<div class="form-row justify-content-center">		
									<div class="form-group col-md-10">
										<input type="text" class="form-control" id="teamname" name="teamname" placeholder="Enter team name" value="<?=$team['team_name']?>">
									</div>
								</div>
								<div class="form-group text-center">
									<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
								</div>
								
							</form>
						</div>
						
					</div>
				</div>
			</div>
			<!------------- End modal------------- -->
			
					<div class="col-sm-4 alert fade show" role="alert">
						<div class="card firstcard">
							<div class="card-body py-4">
								<div class="media ">
									<div class="media-body">
										<h3 class="mb-2 h1 team_name text-center"> <?=$team['team_name'] ?> </h3>
										<div class="mt-4 h5 text-right total-members"> Total Members : 10 </div>
									</div>
								</div>
							</div>
							
											 
							<button type="button" class="close extraclose">
								<span aria-hidden="true"><a href="<?php base_url(); ?>DeleteTeam/<?=$team['team_id']?>">&times;</a></span>
							 </button>
							 
							 <button type="button" class="team-edit">
							   <i class="align-middle mr-2 fas fa-fw fa-edit" data-toggle="modal" data-target="#EditTeam<?=$team['team_id']?>"></i>
							 </button>
						</div>
					</div>
						<?php } ?>
	</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="team_all_btn">
				<div class="btn add-newteam-btn" data-toggle="modal" data-target="#exampleModalCenter">Add New Team</div>
				<div class="btn add-allatten-btn">All Attendees</div>
				<div class="btn un-assignatt-btn">Unassigned Attendees</div>
			</div>

		<!--------------- Modal ---------------->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog  modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title">Add New Team</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>AddTeam" enctype="multipart/form-data">
								<div class="form-row justify-content-center">		
									<div class="form-group col-md-10">
										<input type="text" class="form-control" id="teamname" name="teamname" placeholder="Enter team name">
									</div>
								</div>
								<div class="form-group text-center">
									<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
								</div>
								
							</form>
						</div>
						
					</div>
				</div>
			</div>
			<!------------- End modal------------- -->

		</div>
	</div>
	  <div class="row">
		  <div class="col-12 col-lg-12">
			<div class="card">
				<div class="row">
					<div class="col-12 col-lg-12 mt-minus">
					<div class="table-responsive  border">
					<table class="table mb-0">
					
						<thead>
							<tr>
								<!--<th scope="col">User Id </th>-->
								<th scope="col" style="">User Name</th>
								<th scope="col" style="">Team Name</th>
								<th scope="col" style=""> User Email</th>
								<th scope="col">Phone no.</th>
								<th scope="col">User Address</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach($user_details as $user){
							//echo "<pre>";
							//print_r($user);
							?>
						
							<tr>
								<th scope="row">
								<?=$user['full_name']?>
							</th>
								<td><?php 
									if(!empty($user['team'])){
										
										echo $user['team'][0]['team_name'];
									}else{
										echo "No Team Assigned";
									}
								?></td>
								<td><?=$user['email']?></td>
								<td><?=$user['phone_number']?></td>
								<td><?=$user['street_address']?></td>
								<td>
									
									<i class="align-middle mr-2 fa fa-plus" data-toggle="modal" data-target="#AddAttendee<?=$user['user_id']?>"></i>
									
									<i class="align-middle mr-2 fa fa-edit" data-toggle="modal" data-target="#EditAttendee<?=$user['user_id']?>"></i>
									
									<a href="<?php base_url(); ?>DeleteAttendee/<?=$user['user_id']."@".$user['team'][0]['team_id']?>"><i class="align-middle mr-2 fa fa-times"></i></a>
									
									<a href="" data-toggle="modal" data-target="#user_mng_view"><i class=" align-middle fas fa-eye"></i></a>
								</td>
							</tr>
							<!--------------- Modal ---------------->
								<div class="modal fade" id="AddAttendee<?=$user['user_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleuser-assign-to-team" aria-hidden="true">
									  <div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
										  <div class="modal-header">
												<h3 class="modal-title">Assign to Team</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
										  <div class="modal-body">
											<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>AssignTeam/<?=$user['user_id']?>">
												<select class="custom-select mt-4 mb-4" name="assigned_team">
												<?php
													foreach($teams_details as $team){
														//echo "<pre>";
														//print_r($team);
														
														?>
													  <option value="<?=$team['team_id']?>"><?=$team['team_name']?></option>
													<?php } ?>
													</select>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
												</div>
												
											</form>
										</div>
										 
										</div>
									  </div>
									</div>
						<!------------- End modal------------- -->
						
						
						<!--------------- Edit Modal ---------------->
								<div class="modal fade" id="EditAttendee<?=$user['user_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleuser-assign-to-team" aria-hidden="true">
									  <div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
										  <div class="modal-header">
												<h3 class="modal-title">Assign to Team</h3>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
										  <div class="modal-body">
											<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>EditAssignTeam/<?=$user['user_id']?>">
												<select class="custom-select mt-4 mb-4" name="assigned_team">
											<?php
											foreach($teams_details as $team){
												if(!empty($user['team'])){
								
													if($user['team'][0]['team_name'] == $team['team_name'])
													{?>
													<option selected value="<?=$team['team_id']?>"><?=$team['team_name']?></option>
												<?php			
													}else{?>
													<option value="<?=$team['team_id']?>"><?=$team['team_name']?></option>
													<?php					
													}
												}else{?>
												<option value="<?=$team['team_id']?>"><?=$team['team_name']?></option>
												
											<?php } } ?>
													</select>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-primary center-block btn-lg addstu_btn mt-4"> Submit </button>
												</div>
												
											</form>
										</div>
										 
										</div>
									  </div>
									</div>
						<!------------- End modal------------- -->
						
						
						<?php } ?>
						</tbody>
					</table>

							<!--------------- Modal ---------------->
			<div class="modal fade show" id="user_mng_view" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">

					<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header webclr">
									<h4 class="modal-title clrwhite heading" id="">Team User Details</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">×</span>
									</button>
								  </div>
								<div class="modal-body">
									 <form>
										<div id="formbox">
										  <div class="form-row">
											 <div class="form-group col-md-6">
											  <label for=""> User Id </label>
											  <input type="text" class="form-control" id="" placeholder="DF654">
											</div>
											<div class="form-group col-md-6">
											  <label for=""> User Name </label>
											  <input type="text" class="form-control" id="" placeholder="Chris Wood">
											</div>
										  </div>
										   <div class="form-row">
											<div class="form-group col-md-6">
											  <label for="">User Email</label>
											  <input type="text" class="form-control" id="" placeholder="example@gmail.com">
											</div>
											<div class="form-group col-md-6">
											  <label for="">Phone Number</label>
											  <input type="password" class="form-control" id="" placeholder="+1202-555-0123">
											</div>
										  </div>
										   <div class="form-row">
												<div class="form-group col-md-6">
													<label for="inputAddress">Street Address</label>
													<input type="text" class="form-control" id="" placeholder="1234 Main St">
												</div>
												<div class="form-group col-md-6">
													<label for="inputAddress2">City</label>
													<input type="text" class="form-control" id="" placeholder="Apartment, studio, or floor">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-6">
													<label for="inputCity">State</label>
													<input type="text" class="form-control" id="" placeholder="New York">
												</div>
												<div class="form-group col-md-6">
													<label for="inputState">Zip</label>
													<input type="text" class="form-control" id="" placeholder="Enter Zip code">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-6">
													<label for="inputZip">Country</label>
													<input type="text" class="form-control" id="" placeholder="United States">
												</div>
											</div>

									</div>
									</form>
						</div>
						
					</div>
				</div>
				</div>
			</div>
<!-------- End Modal -------->
				<!---------Start Change Booth modal box ------------------>
				<div class="modal fade" data-keyboard="false" data-backdrop="static" id="change_both" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title"> Change Booth </h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">×</span>
									</button>
								</div>
								<div class="modal-body">
									<form>
										<div id="formbox">
										  <div class="form-row">
											<div class="form-group col-md-6">
											  <label for="">Booth 1</label>
											  <select id="inputState" class="form-control">
												   <option selected="">Select</option>
												   <option>...</option>
												</select>
											</div>
											<div class="form-group col-md-6">
											  <label for="">Booth 2</label>
											   <select id="inputState" class="form-control">
												   <option selected="">Select</option>
												   <option>...</option>
												</select>
											</div>
										  </div>
										   <div class="form-row">
											<div class="form-group col-md-6">
											  <label for="">Time Assigned</label>
											  <input type="text" class="form-control" id="" placeholder="9:00AM">
											</div>
											 <div class="form-group col-md-6">
											  <label for="">Time Assigned</label>
											  <input type="text" class="form-control" id="" placeholder="9:00AM">
											</div>
										  </div>
										  <div class="form-group text-center">
											 <button type="submit" class="btn btn-primary center-block btn-lg addstu_btn"> Update </button>
										  </div>
									</div>
									</form>
								</div>
								
							</div>
						</div>
					</div>
					<!-------------- End Modal box ------------------>

				</div>
		
		</div>
	</div>
			</div>
		</div>
	  </div>
</main>

			<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>

	<script>
	$(document).ready(function (){
	
	$("#attendee_management").addClass("active");
	
	})
	
		$(function() {
			$("#datetimepicker-dashboard").datetimepicker({
				inline: true,
				sideBySide: false,
				format: "L"
			});
		});
	</script>
	<script>
		$(function() {
			// Line chart
			new Chart(document.getElementById("chartjs-dashboard-line"), {
				type: "line",
				data: {
					labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Sales ($)",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.primary,
						data: [2015, 1465, 1487, 1796, 1387, 2123, 2866, 2548, 3902, 4938, 3917, 4927]
					}, {
						label: "Orders",
						fill: true,
						backgroundColor: "transparent",
						borderColor: window.theme.tertiary,
						borderDash: [4, 4],
						data: [928, 734, 626, 893, 921, 1202, 1396, 1232, 1524, 2102, 1506, 1887]
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {
						intersect: false
					},
					hover: {
						intersect: true
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						xAxes: [{
							reverse: true,
							gridLines: {
								color: "rgba(0,0,0,0.05)"
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: 500
							},
							display: true,
							borderDash: [5, 5],
							gridLines: {
								color: "rgba(0,0,0,0)",
								fontColor: "#fff"
							}
						}]
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["Direct", "Affiliate", "E-mail", "Other"],
					datasets: [{
						data: [2602, 1253, 541, 1465],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							"#E8EAED"
						],
						borderColor: "transparent"
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>
	<script>
		$(function() {
			$("#datatables-dashboard-projects").DataTable({
				pageLength: 6,
				lengthChange: false,
				bFilter: false,
				autoWidth: false
			});
		});
	</script>

</body>

</html>