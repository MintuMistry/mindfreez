﻿<main class="content Profile-box">
	<div class="container-fluid p-0">
		<div class="row justify-content-center">
			<div class="col-md-8 col-xl-9">
				<div class="card">
					<div class="edit-profile-img">
						<div class="profile-box">
						<img src="<?php echo base_url()."/writable/uploads/".$admin_data->photo ?>" alt="profile images">
							<span class="edit-pro"><img src="<?php echo base_url(); ?>/assets/images/edit.png"></span>
						</div>
					</div>
					<div class="card-header mt-5">
						<h5 class="card-title mb-0">Personal info</h5>

					</div>
					<div class="card-body">
						<form action="<?php echo base_url('/admin/updateProfile'); ?>/<?=$admin_data->admin_id ?>" method="post" enctype="multipart/form-data">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Full Name</label>
									<input type="text" class="form-control" name="full_name" value="<?=$admin_data->name?>" placeholder="Enter Full Name">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Phone Number</label>
									<input type="text" class="form-control" name="phone_number" value="<?=$admin_data->phone?>" placeholder="Enter Your Phone Number">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Email</label>
									<input type="email" value="<?=$admin_data->admin_email?>" class="form-control" name="email" placeholder="example@gmail.com">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Password</label>
									<input type="password" value="<?=$admin_data->password ?>" class="form-control" name="password" placeholder="**********">
								</div>
							</div>

							<h5 class="card-title">Address</h5>
							<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputAddress">Street Address</label>
								<input type="text" value="<?=$admin_data->street_address ?>" class="form-control" name="street_address" placeholder="1234 Main St">
							</div>
							<div class="form-group col-md-6">
								<label for="inputAddress2">City</label>
								<input type="text" value="<?=$admin_data->city ?>" class="form-control" name="city" placeholder="Apartment, studio, or floor">
							</div>
						</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputCity">State</label>
									<input type="text" value="<?=$admin_data->state ?>" class="form-control" name="state" placeholder="New York">
								</div>
								<div class="form-group col-md-6">
									<label for="inputCity">Zip</label>
									<input type="text" value="<?=$admin_data->zip ?>" class="form-control" name="zip" placeholder="Enter Zip Code">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputZip">Country</label>
									<input type="text" value="<?=$admin_data->country ?>" class="form-control" name="country" placeholder="United States">
								</div>
								
								<div class="form-group col-md-6">
									<label for="inputZip">Choose Photo</label>
									<input type="file" class="form-control"  name="profile" accept="image/*">
								</div>
								</div>
								
								<div class="form-row">
								<div class="form-group col-md-6 text-right">
									<button type="submit" class="btn btn-lg btn-primary logbtn">Save</a></button>
								</div>
							</div>										
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>
</main>
<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="<?php echo base_url(); ?>/assets/js/settings.js"></script> -->
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>

</body>

</html>