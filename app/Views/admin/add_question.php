﻿<?php
	preg_match_all('!\d+!', base_url(uri_string()), $matches);
?>
<main class="content">
	<div class="container-fluid p-0">
	
	<!--------------- Modal ---------------->
		<div class="modal fade show" id="sizedModalSm" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">  Add Sponsor </h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
				<div class="modal-body">
				<div class="row justify-content-md-end pb-3 bordrstyle">
				<div class="form-group col-lg-6 align-self-end mt-2">
						<div class="input-group">
							<input type="text" id="sponsor_search" onkeyup="searchSponsor()" class="form-control" placeholder="Search...">
							<!--<span class="input-group-append">
							  <button class="btn btn-info" type="button">Go!</button>
							</span>-->
						</div>
					</div>
				</div>
				
				<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>/admin/AddGames_Sponsor/<?=$matches[0][0]?>">
					<div id="formbox">
						<div class="table-responsive  border">
					<table class="table mb-0">
						<thead>
							<tr>
								<th><div class="col-lg-1"><div class="col-lg-2 align-self-center" style="margin-left: -15px;"><label class="checkboxuse correct-question">
					<input type="checkbox" onchange="checkAll1(this)" name="chk1[]" ><span class="checkmark"></span></label></div></div></th>
								<th scope="col">Sponsor Image </th>
								<th scope="col">Sponsor Name </th>
							</tr>
						</thead>
						<tbody id="sponsor_box">
						<?php 
						foreach($sponsor_list as $sponsor)
						{?>
			
							<tr>
							<td>
							<div class="col-lg-1 align-self-center text-center"> <label class="checkboxuse correct-question">
							<?php 
							if($sponsor['gameid'] == $matches[0][0])
							{?>
							<input type="checkbox" name="sponsorid[]" value="<?=$sponsor['sponsor_id']?>" checked>
												
							<?php }else{?>
							
							<input type="checkbox" name="sponsorid[]" value="<?=$sponsor['sponsor_id']?>">
							
							<?php
							}
							
							?>
							<span class="checkmark"></span></label>
							</div></td>
								<td scope="row"><img src="<?php echo base_url()."/writable/uploads/".$sponsor['sponsor_img']?>" style="height: 100px; width: 1oopx;" /></td>
								<td><?=$sponsor['sponsor_name'] ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
						  <div class="form-group text-center mt-3">
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn saveclose"> Save</button>
							 
						  </div>
					</div>
				</form>
				</div>
				</div>
			</div>
		</div>
		
		
		<!--------------- Modal ---------------->
		<div class="modal fade show" id="AddQuestions" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">  Add Question </h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
					
				<div class="row justify-content-md-end pb-3 bordrstyle">
				<div class="form-group col-lg-6 align-self-end mt-2">
						<div class="input-group">
							<input type="text" id="question_search" onkeyup="searchQuestion()" class="form-control" placeholder="Search...">
							<!--<span class="input-group-append">
							  <button class="btn btn-info" type="button">Go!</button>
							</span>-->
						</div>
					</div>
				</div>
			
				<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>/admin/AddGames_Question/<?=$matches[0][0]?>">
					<div id="formbox">
						<div class="table-responsive  border">
					<table class="table mb-0">
						<thead>
							<tr>
								<th><div class="col-lg-1"><div class="col-lg-2 align-self-center" style="margin-left: -15px;"><label class="checkboxuse correct-question">
					<input type="checkbox" onchange="checkAll1(this)" name="chk1[]" ><span class="checkmark"></span></label></div></div></th>
								<th scope="col">Video/Image </th>
								<th scope="col">Questions </th>
								<th scope="col">Time </th>
							</tr>
						</thead>
						<tbody id="question_box">
						<?php 
						$i=0;
						foreach($question_list as $questions)
						{
						$i++;
						?>
					<tr>
					<td>
					<div class="col-lg-1 align-self-center text-center"> <label class="checkboxuse correct-question">
					<?php 
					if($questions['gameid'] == $matches[0][0])
					{?>
					<input type="checkbox" name="questionid[]" value="<?=$questions['question_id']?>" checked>
										
					<?php }else{?>
					
					<input type="checkbox" name="questionid[]" value="<?=$questions['question_id']?>">
					
					<?php
					}
					
					?>
					<span class="checkmark"></span></label>
					</div></td>
								<td scope="row"><img src="<?php echo base_url()."/writable/uploads/".$questions['filepath']?>" style="height: 100px; width: 1oopx;" /></td>
								<td><?=$questions['question_text']?></td>
								<td><?=$questions['time_limit']?> Sec</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
						  <div class="form-group text-center mt-3">
							 <button type="submit" class="btn btn-primary  mt-2 addstu_btn saveclose"> Save</button>
							 
						  </div>
					</div>
				</form>
				</div>
				</div>
			</div>
		</div>
		
		
		<div class="row justify-content-md-end pb-3 bordrstyle">
				<div class="form-group col-lg-4 align-self-end mt-2">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-append">
							  <button class="btn btn-info" type="button">Go!</button>
							</span>
						</div>
					</div>
			</div>
			
		<div class="tabclr">
		<div class="row">
			
			<div class="col-lg-1">Sr. No.</div>
			<div class="col-lg-2">Video/Image</div>
			<div class="col-lg-6">Questions/Sponsor</div>
			<div class="col-lg-1">Time</div>
			<div class="col-lg-2 text-center acti-pad">Action</div>
		</div>
	</div>
		<div id="payment_box">
		<div class="accordion" id="accordionExample">
		
		  <?php
		  /* Array
(
    [0] => Array
        (
            [gameshow_id] => 1
            [gameid] => 1
            [ques_id] => 2
            [sponsor_id] => 
            [ques_arr] => Array
                (
                    [0] => Array
                        (
                            [question_id] => 2
                            [question_text] => Who is founder of Google?
                            [time_limit] => 30
                            [filepath] => 20201113/1605246574_462ce10cf465a023acab.jpg
                            [gameid] => 2
                            [explanation] => 
                            [correct_answer] => 
                        )

                )

        )

    [1] => Array
        (
            [gameshow_id] => 2
            [gameid] => 1
            [ques_id] => 
            [sponsor_id] => 5
            [sponsor_arr] => Array
                (
                    [0] => Array
                        (
                            [sponsor_id] => 5
                            [sponsor_name] => Airtel
                            [sponsor_img] => 20201126/1606383551_69c1f8f9046ae56ff663.png
                            [gameid] => 
                        )

                )

        )
 */
		  $i=0;
			foreach($game_question_list as $game_question)
			{
				$i++;
		  ?>
		
		  <div class="card">
			<div class="card-header tab-bodr" id="headingOne">
				<div class="row justify-content-center">
					<div class="col-lg-1 align-self-center"><div class="question"><?=$i?></div> </div>
					<?php
					
					if(!empty($game_question['ques_id'])){?>
					
					<div class="col-lg-2 align-self-center"><div class="vid-img"><img src="<?php echo base_url()."/writable/uploads/".$game_question['ques_arr'][0]['filepath']?>"></div> </div>
					<div class="col-lg-6 align-self-center"><div class="question"><?=$game_question['ques_arr'][0]['question_text']?></div> </div>
					<div class="col-lg-1 align-self-center"><div class="quiestion-time"> <?=$game_question['ques_arr'][0]['time_limit']?> Sec</div> </div>
					
					<?php }else{?>
						
					<div class="col-lg-2 align-self-center"><div class="vid-img"><img src="<?php echo base_url()."/writable/uploads/".$game_question['sponsor_arr'][0]['sponsor_img']?>"></div> </div>
					<div class="col-lg-6 align-self-center"><div class="question"><?=$game_question['sponsor_arr'][0]['sponsor_name']?></div> </div>
					<div class="col-lg-1 align-self-center"><div class="quiestion-time"> </div> </div>
					<?php }?>
					<div class="col-lg-2 align-self-center"><div class="action-pos">
						
						<a href="<?php echo base_url()?>/admin/removeQuestion_or_Sponsor/<?=$matches[0][0]?>@<?=$game_question['gameshow_id']?>"><i class="align-middle mr-2 far fa-fw fa-trash-alt"></i></a>
						<a class="dropicon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<i class="fa fa-angle-down"></i>
							<i class="fa fa-angle-up"></i>
						</a>
					</div> 
					</div>
					
				</div>
			</div>
			
			<?php
			if(!empty($game_question['ques_id'])){
			?>
			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
			  <div class="card-body cardstyle">
				<?=$game_question[0]['explanation']?>
			  </div>
			</div>
			<?php }else{ ?>
			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
			  <div class="card-body cardstyle">
				Sponsor
			  </div>
			</div>
			<?php } ?>
			 </div>
			<?php } ?>
					</div>

					<div class="row">
						<div class="col-lg-3 pr-0">
						<button class="btn btn-info  mt-2 addstu_btn" data-toggle="modal" data-target="#AddQuestions"> Add Question</button>
						
						</div>
						<form method="post" action="<?php echo base_url()?>/admin/setWagerQuestion/<?=$matches[0][0]?>">
						<div class="col-lg-6">
							<div class="do-you-want">Do want to set last question as a Wager question?</div>
							<div class="swichbox">
								<span class="no-swich">No</span>
								<label class="switch">
									
										<input type="checkbox" name="wagerquestion" value="1" checked>
										<span class="slider-swich round"></span>
									
								</label>
								<span class="yes-swich">Yes</span>
							</div>
						</div>
						<div class="col-lg-3">
							<button type="submit" class="btn btn-primary  mt-2 addstu_btn saveclose"> Save and Close Game</button>
						</div>
						</form>
					</div>
					<button class="btn btn-primary  mt-2 addstu_btn" data-toggle="modal" data-target="#sizedModalSm"> Add Sponsor</button>
					<div class="row">
						<div class="col-lg-6 mb-4">
							<a target="_blank" href="<?php echo base_url()?>/admin/StartQuiz/<?=$matches[0][0]?>"><div class="btn startQuiz">Start Quiz</div></a>
						</div>
						<div class="col-lg-6 mb-4">
							<a target="_blank" href="<?php echo base_url()?>/admin/website_management/<?=$matches[0][0]?>"><div class="btn startQuiz">Manage Game Show</div></a>
						</div>
					</div>
		
		</div>
	</div>
</main>
<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/add_input.js"></script>
	
<script>
 function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

 function checkAll1(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 
var i = 0;
var original = document.getElementById('duplicater');
var original1 = document.getElementById('duplicater1');

function duplicate() {
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i;
    // or clone.id = ""; if the divs don't need an ID
    original.parentNode.appendChild(clone);
}

function duplicate1() {
  var itm = document.getElementById("duplicater1").lastChild;
  var cln = itm.cloneNode(true);
  document.getElementById("duplicater1").appendChild(cln);
}


function searchQuestion() {
  // Declare variables
  var input, filter, tbody, tr, td, i, txtValue;
  input = document.getElementById('question_search');
  filter = input.value.toUpperCase();
  tbody = document.getElementById("question_box");
  tr = tbody.getElementsByTagName('tr');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    txtValue = td.textContent || td.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  }
}

function searchSponsor() {
  // Declare variables
  var input, filter, tbody, tr, td, i, txtValue;
  input = document.getElementById('sponsor_search');
  filter = input.value.toUpperCase();
  tbody = document.getElementById("sponsor_box");
  tr = tbody.getElementsByTagName('tr');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    txtValue = td.textContent || td.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  }
}


</script>
</body>

</html>