<?php
use App\Models\Users;
use App\Models\Question;
use App\Models\Sponsor;
?>
<style>
.container{
 border:1px solid darkgrey;
 border-radius:3px;
 padding:5px;
 width: 60%;
 margin: 0 auto;
}

/* Table */
#emp_table {
 border:3px solid lavender;
 border-radius:3px;
}

/* Table header */
.tr_header{
 background-color:dodgerblue;
}
.tr_header th{
 color:white;
 padding:10px 0px;
 letter-spacing: 1px;
}

/* Table rows and columns */
#emp_table td{
 padding:10px;
}
#emp_table tr:nth-child(even){
 background-color:lavender;
 color:black;
}

/* */
#div_pagination{
 width:100%;
 margin-top:5px;
 text-align:center;
}

.button{
 border-radius:3px;
 border:0px;
 background-color:mediumpurple;
 color:white;
 padding:10px 20px;
 letter-spacing: 1px;
}

.divnum_rows{
 display: inline-block;
 text-align: right;
 width: 30%;
}
</style>
<main class="content">
	<div class="container-fluid p-0">
        <?php
            $rowperpage = 1;
            $row = 0;

            // Previous Button
            if(isset($_POST['but_prev'])){
                $row = $_POST['row'];
                $row -= $rowperpage;
                if( $row < 0 ){
                    $row = 0;
                }
            }

            // Next Button
            if(isset($_POST['but_next'])){
                $row = $_POST['row'];
                $allcount = $_POST['allcount'];

                $val = $row + $rowperpage;
                if( $val < $allcount ){
                    $row = $val;
                }
            }
        ?>
    </head>
    <body>
    <div id="content">
        <table width="100%" id="emp_table" border="0">
            <tr class="tr_header">
                <th>Image</th>
                <th>Description</th>
            </tr>
			<?php
            $i=0;
			$gameid = $gameid;
			$allcount = count($question_list);
			$users = new Users();
			$question = new Question();
			$Sponsor = new Sponsor();
			$gameshowid = "";
			$current_question = $users->pagination_link($row,$rowperpage,$gameid);
			//echo "<pre>";
			//print_r($current_question);
			//$gameshowid =$current_question[]['']
			foreach($current_question as $questions)
			{
				$users->set_question_status(array("active_status" => 1),$questions['indexid']);
			
				if(!empty($questions['quesid']))
				{
					//print_r($questions['quesid']);
					$ques_arr = $question->crud_read($questions['quesid']);
					//print_r($ques_arr);
				?>
				<tr>
                    <td align='left'><img src="<?php echo base_url()."/writable/uploads/".$ques_arr[0]['filepath']?>" height="200px" width="200px"></td>
                    <td align='left'><?=$ques_arr[0]['question_text']?></td>
                </tr>
					
				<?php
				}else if(!empty($questions['sponsorid']))
				{
					$sponsor_arr = $Sponsor->crud_read($questions['sponsorid']);
					
					?>
					<tr>
                    <td align='left'><img src="<?php echo base_url()."/writable/uploads/".$sponsor_arr[0]['sponsor_img']?>" height="200px" width="200px"></td>
                    <td align='left'><?=$sponsor_arr[0]['sponsor_name']?></td>
                </tr>
				
					<?php
				}
			}
			?>
        </table>
        <form method="post" action="">
            <div id="div_pagination">
                <input type="hidden" name="row" value="<?php echo $row; ?>">
                <input type="hidden" name="allcount" value="<?php echo $allcount; ?>">
                <button type="submit" class="button" onclick="activate_page();" name="but_prev" ><a href="">Previous</button>
				<button type="submit" class="button" onclick="activate_page();" name="but_next" >Next</button>
            </div>
        </form>
		
    </div>
	</div>
</main>
<!-- <footer class="footer"></footer> -->
		</div>
	</div>
<!-- <script src="js\settings.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/add_input.js"></script>
<script>
/* function activate_page(){
	//var currentrow = <?php echo $current_question[0]['indexid'] ?>;
	//alert(currentrow);
	
} */
</script>