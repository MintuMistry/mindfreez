﻿<?php
	preg_match_all('!\d+!', base_url(uri_string()), $matches);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Bootlab">
	<title> Mind Freeze </title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/classic.css">
</head>

<body>
	<main class="main conexbg">
		<div class="container">
			<div class="row height100vh align-items-center justify-content-center">
				<div class="col-sm-12">
				<div class="login-logo">
					<h1 class="h2 loghed"><img src="<?php echo base_url(); ?>/assets/images/logo.png"></h1>
				</div>
				</div>
				<div class="col-sm-10 col-md-8 col-lg-7">
					<div class="card">
							<div class="card-body">
							
									<div id="loginpage">
											<h4 class="forget-pashd">Forgot your Password?</h4>
											<h6 class="forgblow">Use the form below to recover it.</h6>
										<form action="<?php echo base_url('/admin/login_check'); ?>" method="post">
											<div class="form-group passw mt-5 mb-5">
												<input type="hidden" name="adminid" value="<?=$matches[0][0]?>">
												<input class="form-control form-control-lg" type="password" name="password" placeholder="New Password">
											</div>
											<div class="form-group passw">
												<input class="form-control form-control-lg" type="npassword" name="npassword" placeholder="Confirm New Password">
											</div>
											<div class="text-center mt-3">
											<button type="submit" class="btn btn-lg btn-primary logbtn">Save</a></button>
												
											</div>
										</form>
									</div>
							</div>
						</div>
						
				</div>
			</div>
		</div>
	</main>

	<script src="<?php echo base_url(); ?>/assets/js/app.js"></script>

</body>

</html>