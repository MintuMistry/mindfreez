<?php  

namespace App\Models;

use CodeIgniter\Model;

class Sponsor extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('sponsor');
   		$this->builder1 =  $this->db->table('game_show');
    }

	function crud_read($sponsorid = 0)
	{	
		if($sponsorid>0)
		{
			$this->builder->where('sponsor_id', $sponsorid);
			return $this->builder->get()->getResultArray();
		}else{
			return $this->builder->get()->getResultArray();
		}
		
		
	}
	
	function crud_create($data)
	{	
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_add_game_sponsor($data)
	{	
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	function crud_delete($sponsor_id)
	{	
		$this->builder->where('sponsor_id', $sponsor_id);
		$this->builder->delete();
	
	}
	
	function crud_update($data, $sponsor_id)
	{	
		$this->builder->where("sponsor_id",$sponsor_id);
		$this->builder->update($data);
	}
	

}
?>