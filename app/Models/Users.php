<?php  

namespace App\Models;


use CodeIgniter\Model;

class Users extends Model
{
	protected $db;

    public function __construct()
    {
    
       	$this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users');
   		$this->builder1 =  $this->db->table('teams');
   		$this->builder2 =  $this->db->table('team_attendee');
   		$this->builder3 =  $this->db->table('games');
   		$this->builder4 =  $this->db->table('admin_detail');
		$this->builder5 =  $this->db->table('question');
   		$this->builder6 =  $this->db->table('question_option');
   		$this->builder7 =  $this->db->table('user_answer');
   		$this->builder8 =  $this->db->table('user_result');
   		$this->builder9 =  $this->db->table('team_result');
   		$this->builder10 =  $this->db->table('website_management');
		
    }
	
	function crud_create_website_link($data)
	{
		$this->builder10->insert($data);
		return $this->db->insertID();
	}
	
	function pagination_link($row,$rowperpage,$gameid)
	{
		$response = $this->db->query("SELECT * FROM website_management WHERE game_id =" .$gameid." ORDER BY indexid ASC limit $row,".$rowperpage)->getResultArray();
		return $response;
	}
	
	function set_question_status($data,$indexid)
	{
		//print_r($indexid);
		//die();
		$deactive_all =array("active_status" => 0);
		$this->builder10->update($deactive_all);
		
		$this->builder10->where("indexid",$indexid);
		$this->builder10->update($data);
	}
	
	function crud_delete_website_link($gameid)
	{	
		$this->builder10->where('game_id', $gameid);
		$this->builder10->delete();
	}
	
	function crud_update_admin($data, $admin_id)
	{	
		$this->builder4->where("admin_id",$admin_id);
		$this->builder4->update($data);
	}
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_create_team($data)
	{
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	function crud_create_game($data)
	{
		$this->builder3->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read_game_by_name($game_name)
	{	
		$row = $this->builder3->where("game_name",$game_name)->get()->getRow();
		if (isset($row->game_id)) {
			return $row->game_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_game($game_id = '')
	{	
		if($game_id > 0){
			$this->builder3->where("game_id",$game_id);
			$this->builder3->orderBy('game_id','DESC');
			return $this->builder3->get()->getResultArray();
			
		}
		else{
			$this->builder3->orderBy('game_id','DESC');
			return $this->builder3->get()->getResultArray();
		}
	}
	
	function crud_read($userid = '')
	{	
		if($userid > 0){
			$this->builder->where("user_id",$userid);
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
			
		}
		else{
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
		}
	}
	
	function crud_read_team($teamid = '')
	{	
		if($teamid > 0){
			$this->builder1->where("team_id",$teamid);
			$this->builder1->orderBy('team_id','DESC');
			return $this->builder1->get()->getResultArray();
			
		}
		else{
			$this->builder1->orderBy('team_id','DESC');
			return $this->builder1->get()->getResultArray();
		}
	}
	
	function crud_read_assigned_team($userid)
	{	
		$response = $this->db->query("SELECT team_attendee.*,teams.team_name FROM team_attendee LEFT JOIN teams ON team_attendee.team_id = teams.team_id  WHERE team_attendee.userid =" .$userid)->getResultArray();
		return $response;	
		
		//$this->builder2->where("userid",$userid);
		//return $this->builder2->get()->getResultArray();
	}
	
	function crud_read_team_result($gameid = '')
	{	
	//echo $gameid;
	//die();
		if($gameid > 0){
			$response = $this->db->query("SELECT team_result.*,teams.team_name FROM team_result LEFT JOIN teams ON team_result.teamid = teams.team_id  WHERE team_result.gameid =" .$gameid." ORDER BY gameid")->getResultArray();
			return $response;
		
			/* $this->builder9->where("gameid",$gameid);
			$this->builder9->orderBy('gameid','DESC');
			return $this->builder9->get()->getResultArray(); */
			
		}
		else{
			$response = $this->db->query("SELECT team_result.*,teams.team_name FROM team_result LEFT JOIN teams ON team_result.teamid = teams.team_id ORDER BY gameid")->getResultArray();
			return $response;
			
			//$this->builder9->orderBy('gameid','DESC');
			//return $this->builder9->get()->getResultArray();
		}
	}
	
	function crud_read_result($gameid = '' ,$teamid = '')
	{	
		if($teamid > 0){
			$response = $this->db->query("SELECT user_result.*,users.full_name FROM user_result LEFT JOIN users ON user_result.userid = users.user_id  WHERE user_result.gameid =" .$gameid." AND user_result.teamid =" .$teamid)->getResultArray();
			return $response;
			
			/* $this->builder8->where("gameid",$gameid);
			$this->builder8->where("teamid",$teamid);
			return $this->builder8->get()->getResultArray(); */
			
		}
		else{
			
			$response = $this->db->query("SELECT user_result.*,users.full_name FROM user_result LEFT JOIN users ON user_result.userid = users.user_id  WHERE user_result.gameid =" .$gameid)->getResultArray();
			return $response;
			
			//$this->builder8->where("gameid",$gameid);
			//return $this->builder8->get()->getResultArray();
		}
	}
	
	function crud_read_user_answer($userid = '',$gameid = '')
	{	
		if($userid > 0){
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid." AND user_answer.userid =" .$userid)->getResultArray();
			return $response;	
		}
		else{
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid)->getResultArray();
			return $response;
		}
	}
	
	function crud_read_user_answer_by_question($quesid = '',$gameid = '')
	{	
		if($quesid > 0){
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid." AND user_answer.quesid =" .$quesid)->getResultArray();
			return $response;	
		}
		else{
			
			$response = $this->db->query("SELECT user_answer.*,users.full_name ,teams.team_name ,question_option.option_text FROM user_answer LEFT JOIN users ON user_answer.userid = users.user_id LEFT JOIN teams ON user_answer.teamid = teams.team_id LEFT JOIN question_option ON user_answer.answer_given = question_option.option_id WHERE gameid =" .$gameid)->getResultArray();
			return $response;
		}
	}
	
	function total_user_response($quesid,$gameid)
	{	
		$response = $this->db->query("SELECT answer_id,answer_given FROM user_answer WHERE gameid =" .$gameid." AND quesid =" .$quesid)->getResultArray();
		return $response;	
	}
	function get_users_answer($gameid)
	{	
		$this->builder7->where("gameid",$gameid);
		return $this->builder7->get()->getResultArray();
	}
	
	function get_users_total($gameid,$userid)
	{	
		$this->builder7->where("userid",$userid);
		$this->builder7->where("gameid",$gameid);
		return $this->builder7->get()->getResultArray();
	}
	
	function create_user_result($data)
	{
		$this->builder8->insert($data);
		return $this->db->insertID();
	}
	
	function get_team_total($gameid,$teamid)
	{	
		$this->builder8->where("teamid",$teamid);
		$this->builder8->where("gameid",$gameid);
		return $this->builder8->get()->getResultArray();
	}
	
	function create_team_result($data)
	{
		$this->builder9->insert($data);
		return $this->db->insertID();
	}
	
	function read_team_result($gameid)
	{	
		$this->builder9->where("gameid",$gameid);
		return $this->builder9->get()->getResultArray();
	}
	
	function update_team_rank($data, $score)
	{	
		$this->builder9->where("tscore",$score);
		$this->builder9->update($data);
	}
	
	
	function option_response($optionid,$gameid)
	{	
		$response = $this->db->query("SELECT answer_id,answer_given FROM user_answer WHERE gameid =" .$gameid." AND answer_given =" .$optionid)->getResultArray();
		return $response;	
	}
	
	
	function crud_edit_assign_team($data, $user_id)
	{	
		$this->builder2->where("userid",$user_id);
		$this->builder2->update($data);
	}
	
	function crud_delete_attendee($team_id,$user_id)
	{	
		$this->builder2->where('team_id', $team_id);
		$this->builder2->where("userid",$user_id);
		$this->builder2->delete();
	}
	
	function crud_delete_team($team_id)
	{	
		$this->builder1->where('team_id', $team_id);
		$this->builder1->delete();
	}
	
	function crud_update_team($data, $team_id)
	{	
		$this->builder1->where("team_id",$team_id);
		$this->builder1->update($data);
	}
	
	function crud_assign_team($data)
	{	
		$this->builder2->insert($data);
		return $this->db->insertID();
	}
	

	function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}
	
	function read_pass($oldpass, $user_id)
	{	
		//$response = $this->db->query("SELECT * FROM admin_detail WHERE admin_id =" .$user_id. " AND password =" .$oldpass)->getResultArray();
		//return $response;	
		
		$this->builder7->where('admin_id', $user_id)->where("password", $oldpass);
		return $this->builder7->get()->getResultArray();
	}
	
	function password_update($data, $user_id)
	{	
	
		$this->builder4->where("admin_id",$user_id);
		$this->builder4->update($data);
		//return TRUE;
	}

	function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
		
	}

	function read_by_mail_or_phone($mail,$phone)
	{
		if(!empty($mail))
			return $this->builder->where("email", $mail)
					->get()->getRowArray();
		else
			return $this->builder->where("phone_number", $phone)->get()->getRowArray();

		//return $this->db->query("SELECT * FROM users WHERE user_email='$mail' OR mobile='$phone'")->getResultArray();
	}
	
	function read_by_userid($user_id)
	{
		
		if(!empty($user_id))
		{
			return $this->builder->where("user_id", $user_id)->get()->getResultArray();
		}
	}

	function credentials_check($mail, $phone, $password)
	{
		if(!empty($mail))
			return $this->builder->where("email", $mail)->where("password", $password)
					->get()->getResultArray();
		else
			return $this->builder->where("phone_number", $phone)->where("password", $password)->get()->getResultArray();
	}

	function read_game_by_location($location)
	{
		return $this->builder3->where("game_location", $location)->get()->getResultArray();
		
	}

	function read_game_by_quiz_id($quiz_id)
	{
		return $this->builder3->where("quizid", $quiz_id)->get()->getRowArray();
		
	}
}
?>