<?php  

namespace App\Models;

use CodeIgniter\Model;

class Question extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('question');
   		$this->builder1 =  $this->db->table('question_option');
   		$this->builder2 =  $this->db->table('game_show');
   		$this->builder3 =  $this->db->table('website_management');
    }

	function crud_read($ques_id = '')
	{	
		if($ques_id > 0){
			$this->builder->where("question_id",$ques_id);
			return $this->builder->get()->getResultArray();
		}
		else
			return $this->builder->get()->getResultArray();		
	}
	
	function get_last_question($gameid)
	{	
		$response = $this->db->query("SELECT indexid FROM website_management WHERE game_id =" .$gameid." order by indexid DESC LIMIT 1")->getResultArray();
		return $response;	
	}
	
	
	function crud_read_options($ques_id)
	{	
		$this->builder1->where("question_id",$ques_id);
		return $this->builder1->get()->getResultArray();	
	}
	
	function crud_read_game_question($gameid)
	{	
		$this->builder2->where("gameid",$gameid);
		return $this->builder2->get()->getResultArray();	
		
		
	}
	
	function crud_read_active_game_question($gameid)
	{	
		$this->builder3->where("active_status",1);
		$this->builder3->where("game_id",$gameid);
		return $this->builder3->get()->getResultArray();	
	}
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	
	function crud_create_option($data)
	{
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	function crud_add_game_question($data)
	{
		$this->builder2->insert($data);
		return $this->db->insertID();
	}
	
	function crud_update($data, $ques_id)
	{	
		$this->builder->where("question_id",$ques_id);
		$this->builder->update($data);
	}
	
	function crud_delete($ques_id)
	{	
		$this->builder->where('question_id', $ques_id);
		$this->builder->delete();
		
		$this->builder1->where('question_id', $ques_id);
		$this->builder1->delete();
	}
	
	function crud_delete_option($ques_id)
	{
		$this->builder1->where('question_id', $ques_id);
		$this->builder1->delete();
	}
	
	function crud_delete_gameshow($gameshowid)
	{
		$this->builder2->where('gameshow_id', $gameshowid);
		$this->builder2->delete();
	}
	
	function ques_by_gameid($game_id){
		$this->builder->where('gameid', $game_id);
		return $this->builder->get()->getResultArray();	
	}

}


?>