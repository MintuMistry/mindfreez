<?php 

/*
	Caviar Chat API controller
*/

namespace App\Controllers;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Jwt\Grants\VoiceGrant;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Jwt\ClientToken;
use Twilio\TwiML\VoiceResponse;
use Twilio\TwiML\TwiML;
use CodeIgniter\API\ResponseTrait;
use App\Models\Users;


class Chat extends BaseController
{
	//protected $upload;
	use ResponseTrait;

	private $TWILIO_SID = 'ACb77064a9aeb8c2da4018f0b97a742f04';
	private	$TWILIO_TOKEN = '37be8ca298ce8ac812a5140e99d29421';

	public function index()
	{

		$returndata = array(
			"message" => "Caviar Chat"
		);

		return $this->respond($returndata,200);
	}
	
	public function get_access_token()
	{
			$returndata =array();
			$user_id = $this->request->getPost("user_id");
			$room = $this->request->getPost("room");
			// Use identity and room from query string if provided
			//$identity = isset($_GET["identity"]) ? $_GET["identity"] : "identity";
			//$room = isset($this->request->getPost("room")) ? $this->request->getPost("room") :  "";

			if(!empty($user_id)){
			// Required for all Twilio access tokens
			$twilioAccountSid = 'AC6d8c225b8088bd2bfb7f783511fbb899';
			$twilioApiKey = 'SKa5b6de87043068b18ed81b547c652770';
			$twilioApiSecret = '1PeFl0NnNPx2PZGmXLnCWWn6s25pMvCy';
			$APP_SID = "APbd0c682d1f519f5e66119479e7236e67";
			$PUSH_CREDENTIAL_SID = "CRac9d650beeddabbe81acda5e5ffc2c76";
			// Required for Chat grant
			$serviceSid = 'IScbee3ca0d0a747b89a01b1fb1b4d86b2';
			// choose a random username for the connecting user
			$identity = "Caviar".$user_id;

			// Create access token, which we will serialize and send to the client
			$token = new AccessToken(
				$twilioAccountSid,
				$twilioApiKey,
				$twilioApiSecret,
				3600,
				$identity
			);

			// Create Chat grant
			$chatGrant = new ChatGrant();
			$chatGrant->setServiceSid($serviceSid);
			$grant = new VoiceGrant();
			$grant->setOutgoingApplicationSid($APP_SID);
			$grant->setPushCredentialSid($PUSH_CREDENTIAL_SID);
			$grant->setIncomingAllow(true);
			
			if(!empty($room))
			{
				// Grant access to Video
				$grant = new VideoGrant();
				$grant->setRoom($room);
				$token->addGrant($grant);
			}
				
			
			
			// Add grant to token
			$token->addGrant($chatGrant);
			$token->addGrant($grant);
			
			//echo $token->toJWT();
			// render token to string
			$token = $token->toJWT();
			$returndata['token'] = $token;
			$returndata['identity'] = $identity;
			$users = new Users();
			$users->crud_update(array(
			"access_token" => $token,
			"identity" => $identity
				),$user_id);
				
			$returndata['status'] = "success";
			$returndata['message'] = "Access Token Generated";
			
			}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid user_id";
		}
		return $this->respond($returndata,200);
	}
	
	public function makeCall()
	{
		$sid    = "AC6d8c225b8088bd2bfb7f783511fbb899";
		$token  = "213a7303c22c578599c19889ae7f8abd";
		$twilio = new Client($sid, $token);

		$room = $twilio->video->v1->rooms
								  ->create(["uniqueName" => "DailyStandup"]);

		print($room->sid);
		//$response = new VoiceResponse();
		//$response->say("hello world!", array('voice' => 'alice'));

		//echo $response;

		//$twilioAccountSid = 'AC6d8c225b8088bd2bfb7f783511fbb899';
		//$twilioApiKey = 'SKa5b6de87043068b18ed81b547c652770';
		//$twilioApiSecret = '1PeFl0NnNPx2PZGmXLnCWWn6s25pMvCy';

		// Your Account SID and Auth Token from twilio.com/console
		//$account_sid = 'AC6d8c225b8088bd2bfb7f783511fbb899';
		//$auth_token = '213a7303c22c578599c19889ae7f8abd';
		// In production, these should be environment variables. E.g.:
		// $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

		// A Twilio number you own with Voice capabilities
		//$twilio_number = "+12082739120";
		// $number_verified ="A4RObsUopcbNDMNWQgmhhhTgr8UEa8TGq1jF0W3B";
		// Where to make a voice call (your cell phone?)
		//$to_number = "+919131183079";

		//$client = new Client($account_sid, $auth_token);
		//$client->account->calls->create(  
			//$to_number,
			//$twilio_number,
			//array(
				//"url" => "http://demo.twilio.com/docs/voice.xml"
			//)
		//);


		/* $response = new VoiceResponse;
		$response->say("hello world!", array('voice' => 'alice'));
		print $response;
 */
		/* $callerId = 'client:quick_start';
		$to ="8383976066";
		 $to = isset($_POST["to"]) ? $_POST["to"] : "";
		if (!isset($to) || empty($to)) {
		  $to = isset($_GET["to"]) ? $_GET["to"] : "";
		} 
		
		
		$callerNumber = '1234567890';

		$response = new Twilio\Twiml();
		if (!isset($to) || empty($to)) {
		  $response->say('Congratulations! You have just made your first call! Good bye.');
		} else if (is_numeric($to)) {
		  $dial = $response->dial(
			array(
			  'callerId' => $callerNumber
			));
		  $dial->number($to);
		} else {
		  $dial = $response->dial(
			array(
			   'callerId' => $callerId
			));
		  $dial->client($to);
		}

		print $response; */
	}
	
	public function get_token($user_id)
	{
			// Required for all Twilio access tokens
			$twilioAccountSid = 'AC6d8c225b8088bd2bfb7f783511fbb899';
			$twilioApiKey = 'SKa5b6de87043068b18ed81b547c652770';
			$twilioApiSecret = '1PeFl0NnNPx2PZGmXLnCWWn6s25pMvCy';

			// Required for Chat grant
			$serviceSid = 'IScbee3ca0d0a747b89a01b1fb1b4d86b2';
			// choose a random username for the connecting user
			$identity = "Caviar".$user_id;

			// Create access token, which we will serialize and send to the client
			$token = new AccessToken(
				$twilioAccountSid,
				$twilioApiKey,
				$twilioApiSecret,
				3600,
				$identity
			);

			// Create Chat grant
			$chatGrant = new ChatGrant();
			$chatGrant->setServiceSid($serviceSid);

			// Add grant to token
			$token->addGrant($chatGrant);

			// render token to string
			$token = $token->toJWT();
			$returndata['token'] = $token;
			$returndata['identity'] = $identity;
			$users = new Users();
			$users->crud_update(array(
			"access_token" => $token,
			"identity" => $identity
				),$user_id);
			
			$res_arr =array('identity'=>$identity,'token'=>$token);
			return $res_arr;
		
			
	}
	
	
	public function CreateNewChannel()
	{
			$user_id = "76";
			$token_data = $this->get_token($user_id);
			$token = $token_data['token'];
			$token ="213a7303c22c578599c19889ae7f8abd";
			//echo "<pre>";
			//print_r($token_data);
			//die();
			$returndata =array();
			//$user_id = $this->request->getPost("user_id");
			if(!empty($user_id)){
			// Required for all Twilio access tokens
			$twilioAccountSid = 'AC6d8c225b8088bd2bfb7f783511fbb899';
			$twilioApiKey = 'SKa5b6de87043068b18ed81b547c652770';
			$twilioApiSecret = '1PeFl0NnNPx2PZGmXLnCWWn6s25pMvCy';

			// Required for Chat grant
			$serviceSid = 'IScbee3ca0d0a747b89a01b1fb1b4d86b2';
			// choose a random username for the connecting user
			$identity = "Caviar".$user_id;
			$data = array();
			$data['friendlyName'] ="mintu";
			$data['unique_name'] = "magic";
			$data['type'] = "public";
			//$created_by = "bbbb";
			/* "friendly_name": "friendly_name",
			  "unique_name": "unique_name",
			  "attributes": {
				"foo": "bar"
			  },
			  "type": "public", 
			  "created_by": "username",*/
			  /* ->channels
										->create($data);
			print($channel->sid); */
			
			$twilio = new Client($twilioAccountSid, $token);
			/* $channel = $twilio->chat->v2->services($serviceSid)
			                            ->channels("CH072335ec208c41d987a54bd4a36daabd")
										->fetch();

					print($channel->friendlyName);  done */
				/* $service = $twilio->chat->v2->services
                            ->create($identity);

				print($service->sid);  done */


$user_channel = $twilio->chat->v2->services($serviceSid)
                                 ->users("USd26b8401b3724c1282bc6d923fcc2f73")
                                 ->userChannels("CH20b8bc5cc3f142fd8b9a279bd5c4bafd")
                                 ->fetch();

print_r($user_channel);

			$returndata['status'] = "success";
			$returndata['message'] = "Access Token Generated";
			
			}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid user_id";
		}
		return $this->respond($returndata,200);
	}


}