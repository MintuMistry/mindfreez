<?php 


namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Users;
use App\Models\Question;
use App\Models\Sponsor;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;

class Admin extends BaseController
{

	public function __construct()
    {
        $this->session = session();
		//$this->ciqrcode->generate();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){

			// return redirect()->to();
			header("location: ".base_url('admin')."/login");
			
		}
		else{
			
			return;
		}
	}

	private function loadView($viewname,$data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		//echo view("admin/admin_footer");
	}
	
	private function loadView1($viewname,$data = array())
	{
		
		echo view("admin/$viewname", $data);
		
	}
	
	private function loadWebView($viewname,$data = array())
	{
		echo view("web/header");
		echo view("web/$viewname", $data);
		echo view("web/footer");
		
	}
	
	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@caviar.yesitlabs.xyz"));
	}
	
	public function index()
	{
		$this->checkLogin();
		$users = new Users();
		$data =[];
		$data['game_list'] = $users->crud_read_game();
		$this->loadView("dashboard",$data);	
	}

	public function login()
	{
		echo view("admin/login");
	}

	public function login_check()
	{
		$login = new login();
		if(!empty($this->request->getPost('adminid')))
		{
			$adminid = $this->request->getPost('adminid');
			$users = new Users();
			$users->password_update(
					array("password" => $this->request->getPost("password")), $adminid);
			
			$this->session->set('admin_id', $adminid);
			$this->session->admin_data = $login->admin_data($adminid);
			return redirect()->to(base_url('admin'));
			
		
		}else{
			
			$admin_id = $login->admin_verify(
				$this->request->getPost('email'),
				$this->request->getPost('password')
			);
			if ($admin_id >0){
				$this->session->set('admin_id', $admin_id);
				$this->session->admin_data = $login->admin_data($admin_id);
				return redirect()->to(base_url('admin'));
			}
			else{
				$data['msg'] = "Incorrect Email or Password";
				$this->loadView1("login", $data);
				
			}
		}
	}
	
	public function forgotPassword()
	{
		//$this->checkLogin();
		$users = new Users();
		$data =[];
		$this->loadView1("forgot_password", $data);
	}
	
	public function recoverPassword()
	{
		//$this->checkLogin();
		$login = new login();
		$data =[];
		$admin_id = $login->email_check($this->request->getPost('email'));
		if ($admin_id >0){
			$email = $this->request->getPost('email');
			$msg = base_url('admin/change_pass/'.$admin_id);
			$this->sendMail($email,"Recover Password Link",$msg);
			$data['msg'] = "Change Password Link Sent on your registered email id . Please change password by clicking on that link.";
			$this->loadView1("login", $data);
		}
		else{
			$data['msg'] = "Incorrect Email ID";
			$this->loadView1("forgot_password", $data);
			
		}
	}
	
	
	
	public function change_pass()
	{
		//$this->checkLogin();
		//$users = new Users();
		$data =[];
		$this->loadView1("confirm_password", $data);
	}
	
	public function password_update()
	{
		$this->checkLogin();
		$users = new Users();
		$user_id = $this->session->get('admin_id');
		$check_oldpass = $users->read_pass($this->request->getPost("oldpassword"),$user_id);
		
		if(!empty($check_oldpass)){
			$users->password_update(
					array("password" => $this->request->getPost("password")), $user_id);
			$this->session->setFlashdata('success_msg', "Password changed successfully");
			return redirect()->to(base_url('admin/login') );
		}else{
			$this->session->setFlashdata('success_msg', "Old Password is not correct");
			return redirect()->to(base_url('admin/change_pass') );
		}
		
	}

	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}
	
	/* -------- Game Show Management ------------- */
	
	public function AddGameShow()
	{
		$this->checkLogin();
		$users = new Users();
		$bytes = random_bytes(3);
		$quizid = bin2hex($bytes);
		$game_id = $users->crud_create_game(array(
					"game_name" => $this->request->getPost("game_name"),
					"game_location" => $this->request->getPost("game_location"),
					"game_date" => $this->request->getPost("game_date"),
					"game_time" => $this->request->getPost("game_time"),
					"quizid" => $quizid
				)
			);
		$this->session->setFlashdata('success_msg', "Game Created successfully");
		return redirect()->to(base_url('admin'));
	}
	
	/* ---------Sponsor Management ------------*/
	
	public function sponsor_management()
	{
		$this->checkLogin();
		$data =array();
		$Sponsor = new Sponsor();
		$data['sponsor_list'] = $Sponsor->crud_read();
		/* $users = new Users();
		$user_details_arr = array();
		$user_dtls = $users->crud_read();
		$attribute=array();
		foreach ($user_dtls as $value) {
			
			$value['user_attributes']= $users->read_attributes($value['user_id']);
			$value['user_interest']= $users->read_interest($value['user_id']);
			$user_details_arr[] = $value;
		}
		$data['user_details'] = $user_details_arr; */
		$this->loadView("sponsor_management", $data);
	}
	
	
	public function AddSponsor()
	{
		//sponsor_id	sponsor_name	sponsor_img
		$this->checkLogin();
		$Sponsor = new Sponsor();
		$photopath = "";
		if($img = $this->request->getFile('images'))
		{
			  if ($img->isValid())
			  {
				$photopath = $img->store();
			  } 
		}
				
		$Sponsor_id = $Sponsor->crud_create(array(
					"sponsor_name" => $this->request->getPost("sponsor_name"),
					"sponsor_img" => $photopath
				)
			);
			
			
		$this->session->setFlashdata('success_msg', "Sponsor added successfully");
		return redirect()->to(base_url('admin/sponsor_management'));
	}
	
	public function EditSponsor($Sponsor_id)
	{
		//sponsor_id	sponsor_name	sponsor_img
		$this->checkLogin();
		$Sponsor = new Sponsor();
		$photopath = "";
		if($img = $this->request->getFile('images'))
		{
			  if ($img->isValid())
			  {
				$photopath = $img->store();
			  } 
		}
				
		$Sponsor_id = $Sponsor->crud_update(array(
					"sponsor_name" => $this->request->getPost("sponsor_name"),
					"sponsor_img" => $photopath
				),$Sponsor_id);
			
			
		$this->session->setFlashdata('success_msg', "Sponsor added successfully");
		return redirect()->to(base_url('admin/sponsor_management'));
	}
	
	public function DeleteSponsor($sponsor_id)
	{
		$this->checkLogin();
		$Sponsor = new Sponsor();
		$Sponsor->crud_delete($sponsor_id);
		$this->session->setFlashdata('success_msg', "Sponsor deleted successfully");
		return redirect()->to(base_url('admin/sponsor_management'));
	}
	
	

	/*--------------------------User Management----------------------------*/
	
	public function master_user_management()
	{
		$this->checkLogin();
		$data =array();
		$users = new Users();
		$data['user_details'] = $users->crud_read();
		$this->loadView("master_user_management", $data);
	}
	
	
	public function AddUser()
	{
		$this->checkLogin();
		$users = new Users();
		$user_id = $users->crud_create(array(
					"full_name" => $this->request->getPost("full_name"),
					"phone_number" => $this->request->getPost("phone_number"),
					"email" => $this->request->getPost("email"),
					"password" => $this->request->getPost("password"),
					"street_address" => $this->request->getPost("street_address"),
					"city" => $this->request->getPost("city"),
					"state" => $this->request->getPost("state"),
					"zip" => $this->request->getPost("zip"),
					"country" => $this->request->getPost("country")
				)
			);
			
			
		$this->session->setFlashdata('success_msg', "User added successfully");
		return redirect()->to(base_url('admin/master_user_management'));
	}

	public function editUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$users->crud_update(array(
					"full_name" => $this->request->getPost("full_name"),
					"phone_number" => $this->request->getPost("phone_number"),
					"email" => $this->request->getPost("email"),
					"street_address" => $this->request->getPost("street_address"),
					"city" => $this->request->getPost("city"),
					"state" => $this->request->getPost("state"),
					"zip" => $this->request->getPost("zip"),
					"country" => $this->request->getPost("country"),
					"is_active" => $this->request->getPost("status")
			), $user_id);
			
		$this->session->setFlashdata('success_msg', "User updated successfully");
		return redirect()->to(base_url('admin/master_user_management'));
	}

	public function deleteUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$users->crud_delete($user_id);
		$this->session->setFlashdata('success_msg', "User deleted successfully");
		return redirect()->to(base_url('admin/master_user_management'));
	}
	
	
	
	/* --------------Master Question Management ------------ */
	
	public function master_question_management()
	{
		$this->checkLogin();
		$data =array();
		$question = new Question();
		$question_arr =array();
		$question_list = $question->crud_read();
		foreach($question_list as $questions)
		{
			
			$questions['options'] = $question->crud_read_options($questions['question_id']);
			$question_arr[] = $questions;
			
		}
		//echo "<pre>";
		//print_r($question_arr);
		//die();
		$data['question_list'] = $question_arr;
		$this->loadView("master_question_management", $data);
	}
	
	public function AddQuestion()
	{
		$this->checkLogin();
		$question = new Question();
		$filepath ="";
		/* $answer_arr = $this->request->getPost("correct_answer");
			print_r($answer_arr);
			die(); */
		$customFile = $this->request->getFile('customFile');
		if(!empty($customFile)){
			if($customFile->isValid()){
				$filepath = $customFile->store();
			}
		}
		
		$question_id = $question->crud_create(
			array(
				"question_text" => $this->request->getPost("question"),
				"explanation" => $this->request->getPost("explanation"),
				"time_limit" => $this->request->getPost("time_limit"),
				"filepath" => $filepath
			)
		);
		
		if($question_id > 0){
			$option_arr = $this->request->getPost("option_text");
			$i=0;
			foreach($option_arr as $option)
			{
				if($this->request->getPost("correct_answer") == $i)
				{
					$optionid = $question->crud_create_option(
					array(
						"question_id" => $question_id,
						"option_text" => $option
					));
					
					$question->crud_update(array("correct_answer" => $optionid,), $question_id);
					
				}else{
					
					$question->crud_create_option(
					array(
						"question_id" => $question_id,
						"option_text" => $option
					));
				}
				$i++;
			}
		}
		$this->session->setFlashdata('success_msg', "Question added successfully");
		return redirect()->to(base_url('admin/master_question_management'));
	}

	public function EditQuestion($question_id)
	{
		$this->checkLogin();
		$question = new Question();
		
		$filepath ="";
		$customFile = $this->request->getFile('customFile');
		if(!empty($customFile)){
			if($customFile->isValid()){
				$filepath = $customFile->store();
			}
		}
		if($filepath =="")
		{
			$question->crud_update(array(
				"question_text" => $this->request->getPost("question"),
				"explanation" => $this->request->getPost("explanation"),
				"time_limit" => $this->request->getPost("time_limit")
			), $question_id);
			
		}else{
			$question->crud_update(array(
				"question_text" => $this->request->getPost("question"),
				"time_limit" => $this->request->getPost("time_limit"),
				"filepath" => $filepath
			), $question_id);
		}
		
			
			$option_arr = $this->request->getPost("option_text");
			//print_r($option_arr);
			//die();
			$question->crud_delete_option($question_id);
			$i=0;
			foreach($option_arr as $option)
			{
				if($this->request->getPost("correct_answer") == $i)
				{
					$optionid = $question->crud_create_option(
					array(
						"question_id" => $question_id,
						"option_text" => $option
					));
					
					$question->crud_update(array("correct_answer" => $optionid,), $question_id);
					
				}else{
					
					$question->crud_create_option(
					array(
						"question_id" => $question_id,
						"option_text" => $option
					));
				}
				$i++;
			}
			
		$this->session->setFlashdata('success_msg', "Question updated successfully");
		return redirect()->to(base_url('admin/master_question_management'));
	}

	public function deleteQuestion($ques_id)
	{
		$this->checkLogin();
		$question = new Question();
		$question->crud_delete($ques_id);
		$this->session->setFlashdata('success_msg', "Question deleted successfully");
		return redirect()->to(base_url('admin/master_question_management'));
	}
	
	/* -------manage attendee --------- */
	
	public function manage_attendees()
	{
		$this->checkLogin();
		$data =array();
		$users = new Users();
		$user_details = $users->crud_read();
		$user_arr =array();
		foreach($user_details as $user){
			
			$user['team'] = $users->crud_read_assigned_team($user['user_id']);
			$user_arr[] = $user;
		}
		$data['user_details'] = $user_arr;
		$data['teams_details'] = $users->crud_read_team();
		$this->loadView("manage_attendees", $data);
	}
	
	public function AddTeam()
	{
		$this->checkLogin();
		$users = new Users();
		$teamname = $this->request->getPost('teamname');
		$users->crud_create_team(array("team_name" => $teamname));
		$this->session->setFlashdata('success_msg', "Team created successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	public function DeleteTeam($team_id)
	{
		//echo $team_id;
		//die();
		$this->checkLogin();
		$users = new Users();
		$users->crud_delete_team($team_id);
		$this->session->setFlashdata('success_msg', "Team deleted successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	public function EditTeam($team_id)
	{
		$this->checkLogin();
		$users = new Users();
		$teamname = $this->request->getPost('teamname');
		$users->crud_update_ream(array("team_name" => $teamname),$team_id);
		$this->session->setFlashdata('success_msg', "Team Updated successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	public function AssignTeam($user_id)
	{
		//echo $user_id;
		//die();
		$this->checkLogin();
		$users = new Users();
		$assigned_team = $this->request->getPost('assigned_team');
		$users->crud_assign_team(array("team_id" => $assigned_team,"userid" => $user_id));
		$this->session->setFlashdata('success_msg', "Team Assigned successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	public function EditAssignTeam($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$assigned_team = $this->request->getPost('assigned_team');
		$users->crud_edit_assign_team(array("team_id" => $assigned_team),$user_id);
		$this->session->setFlashdata('success_msg', "Team Attendee updated successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	public function DeleteAttendee($data)
	{
		//echo $data;
		//die();
		$res = explode("@",$data);
		$this->checkLogin();
		$users = new Users();
		$userid = $res[0];
		$teamid = $res[1];
		if(!empty($teamid))
		{
			$users->crud_delete_attendee($teamid,$userid);
		}
		
		$this->session->setFlashdata('success_msg', "Attendee deleted successfully");
		return redirect()->to(base_url('admin/manage_attendees'));
	}
	
	
	
	/*--------------------------Load Views----------------------------*/
	
	public function add_question($gameid)
	{
		$this->checkLogin();
		$data =array();
		$question = new Question();
		$Sponsor = new Sponsor();
		$question_arr =array();
		$question_list = $question->crud_read();
		foreach($question_list as $questions)
		{
			$questions['options'] = $question->crud_read_options($questions['question_id']);
			$question_arr[] = $questions;
			
		}
		$game_question_arr = array();
		$game_question_list = $question->crud_read_game_question($gameid);
		foreach($game_question_list as $game_question)
		{
			if(!empty($game_question['ques_id']))
			{
				$game_question['ques_arr'] = $question->crud_read($game_question['ques_id']);
			
			}else if(!empty($game_question['sponsor_id']))
			{
				$game_question['sponsor_arr'] = $Sponsor->crud_read($game_question['sponsor_id']);
			}
			$game_question_arr[] = $game_question;
		}
		$data['question_list'] = $question_arr;
		$data['sponsor_list'] = $Sponsor->crud_read();
		$data['game_question_list'] = $game_question_arr;
		
		//echo "<pre>";
		//print_r($data['game_question_list']);
		//die();
		$this->loadView("add_question", $data);
	}
	
	public function AddGames_Question($gameid)
	{
		$this->checkLogin();
		$data =array();
		$question = new Question();
		$ques_arr = $this->request->getPost("questionid");
		foreach($ques_arr as $ques)
		{
			//$question->crud_update(array("gameid" => $gameid),$ques);
			$question->crud_add_game_question(array(
			"gameid" => $gameid,
			"ques_id" => $ques
			));
		}
		$this->session->setFlashdata('success_msg', "Question Added to Game successfully");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	public function AddGames_Sponsor($gameid)
	{
		$this->checkLogin();
		$data =array();
		$Sponsor = new Sponsor();
		$sponsor_arr = $this->request->getPost("sponsorid");
		foreach($sponsor_arr as $sponsors)
		{
			$Sponsor->crud_add_game_sponsor(array(
			"gameid" => $gameid,
			"sponsor_id" => $sponsors
			));
			
			//$Sponsor->crud_update(array("gameid" => $gameid),$sponsors);
		}
		$this->session->setFlashdata('success_msg', "Sponsor Added to Game successfully");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	public function setWagerQuestion($gameid)
	{
		$this->checkLogin();
		$users = new Users();
		$res = explode("@",$data);
		$question = new Question();
		if(!empty($this->request->getPost('wagerquestion')))
		{
			$users->crud_update_game(array("wager_ques" => 1),$gameid);
		}else{
			$users->crud_update_game(array("wager_ques" => 0),$gameid);
		}
		
		$this->session->setFlashdata('success_msg', "Last question has been set as wager question.");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	public function removeQuestion_or_Sponsor($data)
	{
		$this->checkLogin();
		$res = explode("@",$data);
		$gameid = $res[0];
		$gameshowid = $res[1];
		$question = new Question();
		//$question->crud_update(array("gameid" => ""),$quesid);
		$question->crud_delete_gameshow($gameshowid);
		$this->session->setFlashdata('success_msg', "Question/Sponsor deleted  From Game successfully");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	
	public function removeQuestion($data)
	{
		$this->checkLogin();
		$res = explode("@",$data);
		$gameid = $res[0];
		$quesid = $res[1];
		$question = new Question();
		$question->crud_update(array("gameid" => ""),$quesid);
		//$question->crud_delete($ques_id);
		$this->session->setFlashdata('success_msg', "Question deleted  From Game successfully");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	public function removeSponsor($data)
	{
		$this->checkLogin();
		$res = explode("@",$data);
		$gameid = $res[0];
		$sponsorid = $res[1];
		$Sponsor = new Sponsor();
		$Sponsor->crud_update(array("gameid" => ""),$sponsorid);
		//$question->crud_delete($ques_id);
		$this->session->setFlashdata('success_msg', "Sponsor deleted  From Game successfully");
		return redirect()->to(base_url('admin/add_question/'.$gameid));
	}
	
	public function profile()
	{
		$this->checkLogin();
		$data =array();
		$login = new login();
		$adminid = $this->session->get('admin_id');
		$data['admin_data'] = $login->admin_data($adminid);
		$this->loadView("profile", $data);
	}
	
	public function updateProfile($adminid)
	{
		$this->checkLogin();
		$users = new Users();
		$filepath ="";
		$photo = $this->request->getFile('profile');
		if(!empty($photo)){
			if($photo->isValid()){
				$filepath = $photo->store();
			}
		}
		
		if($filepath =="")
		{
			$users->crud_update_admin(array(
					"name" => $this->request->getPost("full_name"),
					"phone" => $this->request->getPost("phone_number"),
					"admin_email" => $this->request->getPost("email"),
					"street_address" => $this->request->getPost("street_address"),
					"city" => $this->request->getPost("city"),
					"state" => $this->request->getPost("state"),
					"zip" => $this->request->getPost("zip"),
					"country" => $this->request->getPost("country"),
					"password" => $this->request->getPost("password")
			), $adminid);
			
		}else{
			
		$users->crud_update_admin(array(
					"name" => $this->request->getPost("full_name"),
					"phone" => $this->request->getPost("phone_number"),
					"admin_email" => $this->request->getPost("email"),
					"street_address" => $this->request->getPost("street_address"),
					"city" => $this->request->getPost("city"),
					"state" => $this->request->getPost("state"),
					"zip" => $this->request->getPost("zip"),
					"country" => $this->request->getPost("country"),
					"password" => $this->request->getPost("password"),
					"photo" => $filepath
			), $adminid);
		}
		$this->session->setFlashdata('success_msg', "User updated successfully");
		return redirect()->to(base_url('admin'));
	}
	
	
	public function results()
	{
		$this->checkLogin();
		$data =array();
		$users = new Users();
		$team_result_arr = array();
		$attendee_result_arr = array();
		$question_result_arr = array();
		$gameid = 2;
		$data['error_msg'] = "";
		$game_name = $this->request->getPost("game_name");
		//echo $game_name;
		//die();
		if(!empty($game_name))
		{
			$gameid = $users->crud_read_game_by_name($game_name);
			if ($gameid > 0){
				$gameid = $gameid;
			}else{
				$data['error_msg'] = "Game name is not correct.";
			}
			
		}
		
		$team_result = $users->crud_read_team_result($gameid);
		$attendee_result = $users->crud_read_user_answer("",$gameid);
		$question_result = $users->crud_read_user_answer("",$gameid);
		foreach($team_result as $result)
		{
			$result['user_result'] = $users->crud_read_result($result['gameid'],$result['teamid']);
			$team_result_arr[] = $result;
		}
		$user_arr = array();
		$ques_arr = array();
		foreach($attendee_result as $result)
		{
			$user_arr[] = $result['userid'];
		}
		$user_arr = array_unique($user_arr);
		foreach($user_arr as $userid)
		{
			$attendee_result_arr[] = $users->crud_read_user_answer($userid,$gameid);
		}
		
		$ques_arr = array();
		foreach($question_result as $result)
		{
			$ques_arr[] = $result['quesid'];
		}
		$ques_arr = array_unique($ques_arr);
		foreach($ques_arr as $quesid)
		{
			$question_result_arr[] = $users->crud_read_user_answer_by_question($quesid,$gameid);
		}
		
		
		//echo "<pre>";
		//print_r($question_result);
		//die();
		
		$data['team_result'] = $team_result_arr;
		$data['attendee_result'] = $attendee_result_arr;
		$data['question_result'] = $question_result_arr;
		$data['game_list'] = $users->crud_read_game();
		$this->loadView("results", $data);
	}
	
	public function website_management($gameid)
	{
		$this->checkLogin();
		$data =array();
		$question = new Question();
		$Sponsor = new Sponsor();
		$question_arr =array();
		$data['gameid'] = $gameid;
		$question_list = $question->crud_read();
		foreach($question_list as $questions)
		{
			$questions['options'] = $question->crud_read_options($questions['question_id']);
			$question_arr[] = $questions;
			
		}
		$game_question_arr = array();
		$game_question_list = $question->crud_read_game_question($gameid);
		foreach($game_question_list as $game_question)
		{
			if(!empty($game_question['ques_id']))
			{
				$game_question['ques_arr'] = $question->crud_read($game_question['ques_id']);
			
			}else if(!empty($game_question['sponsor_id']))
			{
				$game_question['sponsor_arr'] = $Sponsor->crud_read($game_question['sponsor_id']);
			}
			$game_question_arr[] = $game_question;
		}
		$data['question_list'] = $question_arr;
		$data['sponsor_list'] = $Sponsor->crud_read();
		$data['game_question_list'] = $game_question_arr;
		
		//echo "<pre>";
		//print_r($data['game_question_list']);
		//die();
		$this->loadView("manage_website", $data);
	}
	
	/* -------- load website ---------- */
	public function StartQuiz($gameid)
	{
		$this->checkLogin();
		$data =array();
		$users = new Users();
		$question = new Question();
		$data['gameid'] = $gameid;
		$data['game_data'] = $users->crud_read_game($gameid);
		
		$game_question_list = $question->crud_read_game_question($gameid);
		$users->crud_delete_website_link($gameid);
		$i=1;
		$j=1;
		
		foreach($game_question_list as $game_question)
		{
			if(!empty($game_question['ques_id']))
			{
				if($i ==1)
				{
					$users->crud_create_website_link(array(
					"ques_no" => $j,
					"game_id" => $game_question['gameid'],
					"quesid" => $game_question['ques_id'],
					"active_status" => 1
					));
					
				}else{
					if($i == count($game_question_list)-1)
					{
						$users->crud_create_website_link(array(
						"ques_no" => $j,
						"game_id" => $game_question['gameid'],
						"quesid" => $game_question['ques_id']
						));
						
						$users->crud_create_website_link(array(
						"ques_no" => $j,
						"game_id" => $game_question['gameid'],
						"quesid" => 1
						));
						
					}else{
						
						$users->crud_create_website_link(array(
						"ques_no" => $j,
						"game_id" => $game_question['gameid'],
						"quesid" => $game_question['ques_id']
						));
					}
				}
				$j++;
			
			}else if(!empty($game_question['sponsor_id']))
			{
				if($i ==1)
				{
					$users->crud_create_website_link(array(
					"game_id" => $game_question['gameid'],
					"sponsorid" => $game_question['sponsor_id'],
					"active_status" => 1
					));
					
				}else{
					
					if($i == count($game_question_list)-1)
					{
						$users->crud_create_website_link(array(
						"game_id" => $game_question['gameid'],
						"sponsorid" => $game_question['sponsor_id']
						));
						
						$users->crud_create_website_link(array(
						"ques_no" => $j,
						"game_id" => $game_question['gameid'],
						"quesid" => 1
						));
						
					}else{
						$users->crud_create_website_link(array(
						"game_id" => $game_question['gameid'],
						"sponsorid" => $game_question['sponsor_id']
						));
					}
				}
			}
			$i++;
		}
		$this->loadWebView("qrcode", $data);
	}
	
	public function home($gameid)
	{
		$this->checkLogin();
		$users = new Users();
		$data =array();
		$data['gameid'] = $gameid;
		$data['game_data'] = $users->crud_read_game($gameid);
		$question = new Question();
		$Sponsor = new Sponsor();
		$question_arr =array();
		$game_question_arr = array();
		$game_question_list = $question->crud_read_active_game_question($gameid);
		
		foreach($game_question_list as $game_question)
		{
			
			if(!empty($game_question['quesid']))
			{
				$question_list = $question->crud_read($game_question['quesid']);
				$i=1;
				foreach($question_list as $questions)
				{
					$questions['options'] = $question->crud_read_options($questions['question_id']);
					$question_arr[] = $questions;
					
					$options = "";
					$alphabet =array('A','B','C','D','E','F','G','H');
					$img_type =array('jpg','png','bmp','jpeg','gif','swf','psd','tiff');
					$image =explode(".",$questions['filepath']);
					if(in_array($image[1],$img_type))
					{
						
						$img = "<div class='header-video-bg'>
						<img src=".base_url()."/writable/uploads/".$questions['filepath']." alt='img' class='img-fluid'>
						</div>";
						
					}else{
						
						$img = "<div class='header-video-bg'>
						<video class='bg-video' poster=".base_url()."/writable/uploads/".$questions['filepath']." autoplay='' loop='' muted=''>
							<source src=".base_url()."/assets/video/vid.mp4>
						</video>
						</div>";
					}
					$j = 0;
					$next_ques = $questions[$i+1]['question_id'];
					foreach($questions['options'] as $option)
					{
					
						$options.= "<a href='".base_url()."/admin/checkAnswer/".$questions['question_id']."@".$next_ques."'><div class='ans-part'><span class='que_num'>".$alphabet[$j]."</span><span class='ans-box'>".$option['option_text']."</span></div></a>";
						$j++;
					}				
	$data['result'] = "<div class='video-question-box' >
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-6 p-0'>
					".$img."
					<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
				</div>
			
			<div class='col-lg-6 p-0'>
					<div id='que_ans_sec'>
						<div class='question'>
							<p>".$questions['question_text']."</p>
						</div>".$options."
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class='percent-sec'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-one'>A</div>
						<div class='progress'>
							<div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-one' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-two'>B</div>
						<div class='progress'>
						  <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-two' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-three'>C</div>
						<div class='progress'>
							 <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-three' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-four'>D</div>
						<div class='progress'>
							 <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-four' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id='footerpart'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-4'>
					<div class='new-quesiton'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_question.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Question</div>
									<div class='point-numb'>01</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4'>
					<div class='new-clock'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_clock.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Clock</div>
									<div class='point-numb count'>
									<span id='timer'>
									<span id='time1'>".$questions['time_limit']."</span> Sec  
								  </span>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4'>
					<div class='new-points'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_point.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Points</div>
									<div class='point-numb'>100</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>@@question".$questions['question_id']."@@".$questions['time_limit'];
	$i++;
	}
			
			}else if(!empty($game_question['sponsorid']))
			{
				$game_question['sponsor_arr'] = $Sponsor->crud_read($game_question['sponsorid']);
				
				$data['result'] = "<div class='video-question-box' >
							<div class='container-fluid'>
								<div class='row'>
									<div class='col-lg-12 p-0'>
										<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
										
										<img src=".base_url()."/writable/uploads/".$game_question['sponsor_arr'][0]['sponsor_img']." alt='Sponsor' class='img-fluid'>
									</div>
								</div>
							</div>
						</div>@@sponsor".$game_question['sponsorid'];
			}
			
		}
		$this->loadWebView("home", $data);
	}
	public function get_new_question($gameid)
	{
		$this->checkLogin();
		$users = new Users();
		$data =array();
		$data['gameid'] = $gameid;
		$data['game_data'] = $users->crud_read_game($gameid);
		$question = new Question();
		$Sponsor = new Sponsor();
		$question_arr =array();
		$game_question_arr = array();
		$result ="";
		$game_question_list = $question->crud_read_active_game_question($gameid);
		
		foreach($game_question_list as $game_question)
		{
			if(!empty($game_question['quesid']))
			{
				if($game_question['quesid'] > 1)
				{
				$question_list = $question->crud_read($game_question['quesid']);
				$i=1;
				foreach($question_list as $questions)
				{
					$questions['options'] = $question->crud_read_options($questions['question_id']);
					$question_arr[] = $questions;
					
					$options = "";
					$alphabet =array('A','B','C','D','E','F','G','H');
					$j = 0;
					$img_type =array('jpg','png','bmp','jpeg','gif','swf','psd','tiff');
					$image =explode(".",$questions['filepath']);
					if(in_array($image[1],$img_type))
					{
						
						$img = "<div class='header-video-bg'>
						<img src=".base_url()."/writable/uploads/".$questions['filepath']." alt='img' class='img-fluid'>
						</div>";
						
					}else{
						
						$img = "<div class='header-video-bg'>
						<video class='bg-video' poster=".base_url()."/writable/uploads/".$questions['filepath']." autoplay='' loop='' muted=''>
							<source src=".base_url()."/assets/video/vid.mp4>
						</video>
						</div>";
					}
					foreach($questions['options'] as $option)
					{
					
						$options.= "<a href='".base_url()."/admin/checkAnswer/".$questions['question_id']."@".$next_ques."'><div class='ans-part'><span class='que_num'>".$alphabet[$j]."</span><span class='ans-box'>".$option['option_text']."</span></div></a>";
						$j++;
					}				
	$result = "<div class='video-question-box' >
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-6 p-0'>
					".$img."
					<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
				</div>
			
			<div class='col-lg-6 p-0'>
					<div id='que_ans_sec'>
						<div class='question'>
							<p>".$questions['question_text']."</p>
						</div>".$options."
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class='percent-sec'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-one'>A</div>
						<div class='progress'>
							<div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-one' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-two'>B</div>
						<div class='progress'>
						  <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-two' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-three'>C</div>
						<div class='progress'>
							 <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-three' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
				<div class='col-lg-3'>
					<div class='percentag-box'>
						<div class='progress-bar-num color-four'>D</div>
						<div class='progress'>
							 <div class='progress-percentage' style='width:0%; text-align: right;'>0%</div>
						  <div class='progress-bar color-four' role='progressbar' style='width: 0%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id='footerpart'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-4'>
					<div class='new-quesiton'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_question.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Question</div>
									<div class='point-numb'>01</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4'>
					<div class='new-clock'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_clock.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Clock</div>
									<div class='point-numb count'>
									<span id='timer'>
									<span id='time1'>".$questions['time_limit']."</span> Sec  
								  </span>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4'>
					<div class='new-points'>
						<div class='row'>
							<div class='col-lg-6'> 
								<div class='foot-img-style text-right'><img src=".base_url()."/assets/images/footer_point.png alt='images' class='img-fluid'></div></div> 
							<div class='col-lg-6'>
								<div class='pointbox'>
									<div class='point'>Points</div>
									<div class='point-numb'>100</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>@@question".$questions['question_id']."@@".$questions['time_limit'];
	$i++;
	}
				}else{
					// bid percentage question box 
					$result = "<div class='video-question-box' >
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-12 p-0'>
					".$img."
					<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
				</div>
				</div>
				<div class='select-bid-prcent-section'>
				<div class='row'>
					<div class='col-lg-12'>
						<div class='bid_perc'>Select Your Bid Percentage </div>
						<div class='bitpercentage-box'><span class='bit-per'>0%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>25%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>50%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>75%</span></div>
						<div class='bitpercentage-box'><span class='bit-per'>100%</span></div>
					</div>
				</div>			
			</div>
		</div>
	</div>@@question".$game_question['quesid']."@@0";
					
			}
			}else if(!empty($game_question['sponsorid']))
			{
				$game_question['sponsor_arr'] = $Sponsor->crud_read($game_question['sponsorid']);
				
				$result = "<div class='video-question-box'>
							<div class='container-fluid'>
								<div class='row'>
									<div class='col-lg-12 p-0'>
										<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
										
										<img src=".base_url()."/writable/uploads/".$game_question['sponsor_arr'][0]['sponsor_img']." alt='Sponsor' class='img-fluid'>
									</div>
								</div>
							</div>
						</div>@@sponsor".$game_question['sponsorid'];
			}
			
		}
		echo $result;
	}
	
	public function checkAnswer($gameid)
	{
		$this->checkLogin();
		$users = new Users();
		$question = new Question();
		$Sponsor = new Sponsor();
		$question_arr =array();
		$game_question_arr = array();
		$result ="";
		$game_question_list = $question->crud_read_active_game_question($gameid);
		$ques_status = 0;
		$last_question = $question->get_last_question($gameid);
		
		foreach($game_question_list as $game_question)
		{
			//print_r($game_question);
			//die();
			if(!empty($game_question['quesid']))
			{
				if($last_question[0]['indexid'] == $game_question['indexid'])
				{
					$users_answer = $users->get_users_answer($gameid);
					$user_arr = array();
					$team_arr = array();
					$ques_status = 1;
					foreach($users_answer as $user_answer)
					{
						$user_arr[] = $user_answer['userid'];
						$team_arr[] = $user_answer['teamid'];
					}
					array_unique($user_arr);
					
					foreach($user_arr as $attendee)
					{
						$user_total_arr = $users->get_users_total($gameid,$attendee);
						$total_points = 0;
						$total_wrong_answer = 0;
						$total_right_answer = 0;
						
						foreach($user_total_arr as $total_arr)
						{
							$total_points += $total_arr['points'];
							if($total_arr['answer_status'] >0)
							{
								$total_right_answer += 1;
								
							}else{
								$total_wrong_answer += 1;
							}
						}
						$user_result = $users->create_user_result(array(
						"gameid" => $gameid,
						"teamid" => $user_total_arr[0]['teamid'],
						"userid" => $attendee,
						"total_score" => $total_points,
						"total_points" => $total_points,
						"total_correct_answer" => $total_right_answer
						));
						
					}
					
					array_unique($team_arr);
					
					foreach($team_arr as $team)
					{
						$team_total_arr = $users->get_team_total($gameid,$team);
						$team_points = 0;
						
						foreach($team_total_arr as $total_arr)
						{
							$total_points += $total_arr['points'];
							
						}
						$team_result = $users->create_team_result(array(
						"gameid" => $gameid,
						"teamid" => $team,
						"tscore" => $team_points,
						"tpoints" => $total_points
						));
						
					}
					
					$team_result = $users->read_team_result($gameid);
					$team_score_arr =array();
					foreach($team_result as $result)
					{
						$team_score_arr[] = $result['tpoints'];
					}
					rsort($team_score_arr);
					$j=1;
					foreach($team_score_arr as $value)
					{
						$team_rank = $users->update_team_rank(array("rank" => $j),$value);
						$j++;
					}
				}
				$question_list = $question->crud_read($game_question['quesid']);
				$i=1;
				$poll_percentage = array("40","30","10","20");
				foreach($question_list as $questions)
				{
					$questions['options'] = $question->crud_read_options($questions['question_id']);
					//$question_arr[] = $questions;
					
					$ques_option = "";
					$poll_response = "";
					$poll = 0;
					$alphabet =array('A','B','C','D','E','F','G','H');
					$color_arr = array('color-one','color-two','color-three','color-four');
					$j = 0;
					$img_type =array('jpg','png','bmp','jpeg','gif','swf','psd','tiff');
					$image =explode(".",$questions['filepath']);
					if(in_array($image[1],$img_type))
					{
						
						$img = "<div class='header-video-bg'>
						<img src=".base_url()."/writable/uploads/".$questions['filepath']." alt='img' class='img-fluid'>
						</div>";
						
					}else{
						
						$img = "<div class='header-video-bg'>
						<video class='bg-video' poster=".base_url()."/writable/uploads/".$questions['filepath']." autoplay='' loop='' muted=''>
							<source src=".base_url()."/assets/video/vid.mp4>
						</video>
						</div>";
					}
					$poll_arr = array();
					$user_response_arr = $users->total_user_response($questions['question_id'],$gameid);
					
					$total_user_response = count($user_response_arr);
					//echo "<pre>";
					//print_r($user_response_arr);
					//die();
					foreach($questions['options'] as $option)
					{
						$questions['options'] = $question->crud_read_options($questions['question_id']);
						$poll_arr[] = $option['poll'];
					}
					//print_r($poll_arr);
					//die();
					foreach($questions['options'] as $option)
					{
						
						$total_response = count($users->option_response($option['option_id'],$gameid));
						if($total_response >0)
						{
							$poll = round(($total_response/$total_user_response)*100);
						}else{
							$poll = 0;
						}
						//echo $poll;
						//die();
						$srno = $alphabet[$j];
						//$poll_arr[] = $option['poll'];
						$ques_option.="<div class='ans-part'><span class='que_num'>".$srno."</span><span class='ans-box'>".$option['option_text']."</span>";
						
						if($questions['correct_answer'] == $option['option_id']){
							
							$ques_option.="<span class='correct_ans'><img src='".base_url()."/assets/images/right_ans.png'></span>";
						}else{ 
							$ques_option.="<span class='wrong_ans'><img src='".base_url()."/assets/images/wrong_ans.png'></span>";
						}
							$ques_option.="</div>";

						$poll_response.= "<div class='col-lg-3'>
							<div class='percentag-box'>
								<div class='progress-bar-num ".$color_arr[$j]."'>".$srno."</div>
								<div class='progress'>
									<div class='progress-percentage' style='width:'".$poll."%' text-align: right;'>".$poll."%</div>
								  <div class='progress-bar color-one' role='progressbar' style='width:".$poll."%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
								</div>
							</div>
						</div>";

						$j++;
						//echo $j;
						//die();
					}
					
					$result = "<div class='video-question-box' >
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-6 p-0'>
					".$img."
					<div class='logo'><img src=".base_url()."/assets/images/logo.png alt='logo' class='img-fluid'></div>
				</div>
				<div class='col-lg-6 p-0'>
					<div id='que_ans_sec'>
						<div class='question'>
							<p>".$questions['question_text']."</p>
						</div>
						".$ques_option."
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class='percent-sec'>
		<div class='container-fluid'>
			<div class='row'>
			".$poll_response."
			</div>
		</div>
	</div>

	<div id='footerpart'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-lg-12'>
					<div class='new-quesiton'>
						<div class='row'>
							<div class='col-lg-6'> 
								".$questions['explanation']."
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>@@question".$questions['question_id']."@@".$ques_status;
				}
			}else{
				continue;
			}
		}			
		echo $result;
	}
	
	
	public function bidPercentage()
	{
		$this->checkLogin();
		$data =array();
		$this->loadWebView("bid_percentage", $data);
	}
	
	public function winnerResult($gameid)
	{
		$this->checkLogin();
		$data =array();
		$users = new Users();
		$team_result_arr = array();
		$team_result = $users->crud_read_team_result($gameid);
		foreach($team_result as $result)
		{
			$result['user_result'] = $users->crud_read_result($result['gameid'],$result['teamid']);
			$team_result_arr[] = $result;
		}
		$data['team_result'] = $team_result_arr;
		$this->loadWebView("winner_result", $data);
	}
	
	
}
