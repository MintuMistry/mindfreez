<?php 
/*
	Caviar Main API controller
*/


namespace App\Controllers;
use CodeIgniter\I18n\Time;
use Twilio\Rest\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;
use App\Models\Users;
use App\Models\Subscription;
use App\Models\Content;
//use App\Models\Question_answer;
use App\Models\Contactus;
//use App\Models\Event;

class Api extends BaseController
{
	//protected $upload;
	use ResponseTrait;

	private $TWILIO_SID = 'ACb77064a9aeb8c2da4018f0b97a742f04';
	private	$TWILIO_TOKEN = '37be8ca298ce8ac812a5140e99d29421';

	private function generateOTP()
	{
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 

	    return $otp;
	}

	public function index()
	{

		$returndata = array(
			"message" => "Caviar API"
		);

		return $this->respond($returndata,200);
	}

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);

	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12673949525',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally{
			return 0;
		}
			
	}

	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@caviar.yesitlabs.xyz"));
	}

	public function send_otp()
	{
		$returndata = array();

		$mobile = $this->request->getPost("mobile");
		$email = $this->request->getPost("email");
		$country_code = $this->request->getPost("country_code");

		$users = new Users();
		$userdata = $users->read_by_mail_or_phone($email,$mobile,$country_code);
		//print_r($userdata);
		//echo $users->getLastQuery();
		// if(count($userdata) < 1){
		// 	$returndata['status'] = "error";
	  //       	$returndata['otp'] = "Invalid User!";
	  //       	return $this->respond($returndata,200);
		// }
		
		$code = $this->generateOTP();
		$msg = "Your otp for Caviar is $code";

		if($email != ""){
			$this->sendMail($email,"OTP for Caviar",$msg);
		}
		elseif($mobile != ""){
	
			$resp = $this->sendTwilioSMS($msg,$mobile);
			// print_r($resp);
		}

        $returndata['status'] = "success";
        $returndata['otp'] = $code;
		
        if(!empty($userdata)){
       		$returndata['user_id'] = $userdata[0]['user_id'];
			if($userdata[0]['is_active'] != 1)
			{
				//$new_arr =array();
				//$new_arr['status'] = "error";
	         	//$new_arr['message'] = "Inactive User!";
	         	$returndata['message'] = "Inactive User!";
				
				//return $this->respond($new_arr,200);
			}
		}
        return $this->respond($returndata,200);

	}

	public function verify_profile_send_otp()
	{
		$returndata = array();

		$user_id = $this->request->getPost("user_id");
		//$email = $this->request->getPost("email");

		$users = new Users();
		$userdata = $users->read_by_userid($user_id);
		//print_r($userdata);
		//die();
		if(count($userdata) < 1){
			$returndata['status'] = "error";
        	$returndata['otp'] = "Invalid User!";
        	return $this->respond($returndata,200);
		}
		$userdata = $userdata[0];
		
		$code = $this->generateOTP();
		$msg = "Your otp for Caviar is $code";

		 if($userdata['user_email'] != ""){
			$this->sendMail($userdata['user_email'],"OTP for Caviar",$msg);
			
		}
		elseif($userdata['mobile'] != ""){
			$resp = $this->sendTwilioSMS($msg,$userdata['mobile']);
			// print_r($resp);
		} 

		
		$users->crud_update(array("verified" => 1),$userdata['user_id']);

        $returndata['status'] = "success";
        $returndata['otp'] = $code;

        return $this->respond($returndata,200);

	}
	
	public function verify_yourself()
	{
		$returndata = array();

		$user_id = $this->request->getPost("user_id");
		$photo_format_id = $this->request->getPost("photo_format_id");
		//$user_photo = $this->request->getPost("user_photo");
		
		$users = new Users();
		$userdata = $users->read_by_userid($user_id);
		if(count($userdata) < 1){
			$returndata['status'] = "error";
        	$returndata['otp'] = "Invalid User!";
        	return $this->respond($returndata,200);
		}else{
			
			$user_photo = $this->request->getFile('user_photo');
				if(!empty($user_photo)){
					if($user_photo->isValid()){
						$photo_imgpath = $user_photo->store();
							
						if($photo_imgpath != ""){
						$photoid1 = $users->user_photo_verification(array(
							"user_photo" => $photo_imgpath,
							"userid" => $user_id,
							"photo_format" => $photo_format_id,
							"verify_status" => 0
						));
						
						}

						/* if($photoid1 > 0){
							$res = $users->delete_user_profile($user_data[0]['profile_imageid']);
							// save and use as profile image
							$users->crud_update(array("profile_imageid" => $photoid1),$user_id);
						}  */
					
					}
				}
				
					
						//echo $users->getLastQuery();
						//echo "<pre>";
						//print_r($res);
						//echo "<br/>";
						//die();
			//$userdata = $userdata[0];
			//$users->crud_update(array("verified" => 1),$userdata['user_id']);

			$returndata['status'] = "success";
			$returndata['message'] = "Data Inserted";
			return $this->respond($returndata,200);
		}
	}
	
	/*-------------Reporte User API---------------*/
	public function report_user()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		$reported_user_id = $this->request->getPost('reported_user_id');
		$report_msg = $this->request->getPost('report_msg');
		$users->report_user(array(
							"userid" => $user_id,
							"reported_user_id" => $reported_user_id,
							"report_msg" => $report_msg
						));
		$reported_count = count($users->read_report_user($reported_user_id,$user_id));
			if($reported_count >= 3 )
			{
				$users->crud_update(array(
							"reported_count" => $reported_count,
							"is_active" => 0
							),$reported_user_id);
				
			}else{
				$users->crud_update(array("reported_count" => $reported_count),$reported_user_id);
			}
		
		//$users->crud_update(array("user_status",3)$user_id);	
		$returndata['status'] = "success";
		$returndata['message'] = "User reported";
		return $this->respond($returndata,200);
	}
	
	/*-------------Block User API---------------*/
	public function block_user()
	{
		$returndata = array();
		$users = new Users();
		
		$users->create_block_users(array(
			"userid" => $this->request->getPost('user_id'),
			"block_userid" => $this->request->getPost('block_user_id')
		));
		$returndata['status'] = "success";
		$returndata['message'] = "User blocked";
		return $this->respond($returndata,200);
	}

	/*-------------Unblock User API---------------*/
	public function unblock_user()
	{
		$returndata = array();
		$users = new Users();
		$userid = $this->request->getPost('user_id');
		$block_userid = $this->request->getPost('block_user_id');
		$users->delete_block_users_api($userid, $block_userid);
		$returndata['status'] = "success";
		$returndata['message'] = "User unblocked";
		
		return $this->respond($returndata,200);
	}
	
	public function login()
	{
		
		$returndata = array();

		$mobile = $this->request->getPost("mobile");
		$email = $this->request->getPost("email");
		$country_code = $this->request->getPost("country_code");
		$users = new Users();
		$userdata = $users->read_by_mail_or_phone($email,$mobile,$country_code);
		//print_r($userdata);
		//die();
		 if(count($userdata) < 1){
			$returndata['status'] = "error";
        	$returndata['message'] = "Invalid User!";
        	return $this->respond($returndata,200);
		}
		else{
			$userdata = $userdata[0];
			if($userdata['is_active'] == 0){
				$returndata['status'] = "error";
	        	$returndata['otp'] = "User is inactive";
			}
			else{
				$returndata['status'] = "success";
	        	$returndata['otp'] = "Login success!";
			}

        	return $this->respond($returndata,200);
		} 
	}

	public function logout_delete()
	{
		$returndata = array();
		$type = $this->request->getPost("type");
		$user_id = $this->request->getPost("user_id");
		$users = new Users();
		$userdata = $users->crud_read($user_id);
		if(count($userdata) < 1){
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid User!";
			return $this->respond($returndata,200);
		}

		$userdata = $userdata[0];
		//print_r($userdata);
		
		if($type == 1){
			$users->crud_update(array("user_status" => 0),$user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Logged out successfully";
		}
		elseif($type == 2){
			$users->crud_update(array("user_status" => 3),$user_id);
			$users->crud_delete($user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Account Deleted successfully";
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid Type!";
		}

		return $this->respond($returndata,200);
	}

	/*-------------Privacy Policy & T&C API---------------*/
 	public function get_content()
	{
		//echo "sdegcjsdh";
		//die();
		$returndata = array();
		$content = new Content();
		$content_data = $content->crud_read();
		//print_r($content_data);die;
		$new_arr = array();
		foreach ($content_data as $data) {
			if($data['content_id'] =="1")
			{
				$new_arr['Privacy_policies']= array(
					"page_name" => $data['page_name'],
					"page_content" => preg_replace("/\s+/", " ", $data['page_content'])
				);
				
			}else if($data['content_id'] =="2")
			{
				$new_arr['Terms_and_conditions']= array(
					"page_name" => $data['page_name'],
					"page_content" => preg_replace("/\s+/", " ", $data['page_content'])
				);
			}
			
					
		}
		$returndata['content_data'] = $new_arr;
		$returndata['status'] = "success";
		$returndata['message'] = "Content list";
		return $this->respond($returndata,200);
	}
	
	
	/*-------------Subscription plan API---------------*/
	public function get_subscription_plan()
	{
		$returndata = array();
		$subscriptions = new Subscription();
		$subscription_data = $subscriptions->crud_read();
		//print_r($subscription_data);die;
		$new_arr = array();
		foreach ($subscription_data as $data) {
			$new_arr[] = $data['plan_name'];
		}
		$returndata['subscription_plan'] = $subscription_data;
		$returndata['status'] = "success";
		$returndata['message'] = "Subscription plan";
		return $this->respond($returndata,200);
	} 
	
	/*------------- User Registration API---------------*/
	public function register_user()
	{
		$returndata = array();
		$users = new Users();
		//print_r($this->request->getPost());
		//echo $this->request->getPost("mobile");
		//die();
		//$country_code => $this->request->getPost("country_code");
		$userdata = $users->read_by_mail_or_phone($this->request->getPost("email"),$this->request->getPost("mobile"),$this->request->getPost("country_code"));
		//echo $users->getLastQuery();
		//die();
		
		if(count($userdata) > 0){
			$returndata['status'] = "error";
        	$returndata['message'] = "User registered already!";

        	return $this->respond($returndata,200);
		}
		
		$userid = $users->crud_create(array(
			"fcmtoken" => $this->request->getPost("fcm_token"),
			"country_code" => $this->request->getPost("country_code"),
			"mobile" => $this->request->getPost("mobile"),
			"user_email" => $this->request->getPost("email"),
			"name" => $this->request->getPost("first_name"),
			"lname" => $this->request->getPost("last_name"),
			"address" => $this->request->getPost("address"),
			"latitude" => $this->request->getPost("lat"),
			"longitude" => $this->request->getPost("long"),
			"living_in" => $this->request->getPost("living_in"),
			"nationality" => $this->request->getPost("nationality"),
			"gender" => $this->request->getPost("gender"), //M: Male, F: Female
			"interestedin" => $this->request->getPost("looking_for"),
			"is_active" => 1
		));
		if($userid < 1){
			$returndata['status'] = "error";
			$returndata['message'] = "REGISTRATION FAILURE!";
			return $this->respond($returndata,200);
		}
		$users->add_attributes(
			array(
				"age" => $this->request->getPost("age"),
				"relationship_status" => $this->request->getPost("relationship_status"),
				"relationship_looking_for" => $this->request->getPost("relationship_looking_for"),
				"religion" => $this->request->getPost("religion"),
				"body_type" => $this->request->getPost("body_type"),
				"height" => $this->request->getPost("height"),
				"job_status" => $this->request->getPost("job_status"),
				"education" => $this->request->getPost("education"),
				"housing" => $this->request->getPost("housing"),
				"drink" => $this->request->getPost("drink"),
				"smoke" => $this->request->getPost("smoke")
			),$userid);
			
			$users->add_seekattributes(
			array(
				"age" => $this->request->getPost("seeking_age"),
				"relationship_status" => $this->request->getPost("relationship_status_looking_for"),
				"relationship" => $this->request->getPost("relationship_looking_for"),
				"nationality" => $this->request->getPost("seeking_nationality"),
				"living_in" => $this->request->getPost("seeking_living_in"),
				"body_type" => $this->request->getPost("seeking_bodytype"),
				"height" => $this->request->getPost("seeking_height"),
				"job_status" => $this->request->getPost("seeking_job_status"),
				"education" => $this->request->getPost("seeking_education"),
				"living_situation" => $this->request->getPost("seeking_living_situation"),
				"drink" => $this->request->getPost("seeking_drink"),
				"smoke" => $this->request->getPost("seeking_smoke"),
				"describe_yourself" => $this->request->getPost("describe_yourself"),
				"religion" => $this->request->getPost("seeking_religion")
			),$userid);
		
		
		// profile image
		$photofile = $this->request->getFile('profile_img');
			if($photofile){
				if($photofile->isValid()){
					$photopath = $photofile->store();
						
					if($photopath != ""){
						$photoid = $users->add_image(array(
							"image_path" => $photopath,
							"userid" => $userid,
							"image_type" => "Profile"
						));
						if($photoid > 0){
							$users->crud_update(array("profile_imageid" => $photoid),$userid);
						}
					}
				}
			}
			
		// get image list
		for($i = 1;$i <= 6;$i++){
			$photofile = $this->request->getFile('Photo'.$i);
			if($photofile){
				if($photofile->isValid()){
					$photopath = $photofile->store();
						
					if($photopath != ""){
						$photoid = $users->add_image(array(
							"image_path" => $photopath,
							"userid" => $userid,
							"image_type" => "gallery"
						));
					}
				}
			}
		}
		$returndata['status'] = "success";
		$returndata['message'] = "User registered successfully";
		$returndata['user_id'] = $userid;

		return $this->respond($returndata,200);
	}
	
	
	/*-------------Update User Profile API---------------*/
	public function update_profile_data()
	{
		$returndata = array();
		$user_id = $this->request->getPost('user_id');
		$users = new Users();
		$user_data = $users->crud_read($user_id);
		//print_r($user_data);
		//die();
		if(!empty($user_data)){
			
			$users->crud_update(array(
				"fcmtoken" => $this->request->getPost("fcm_token"),
				"country_code" => $this->request->getPost("country_code"),
				"mobile" => $this->request->getPost("mobile"),
				"user_email" => $this->request->getPost("email"),
				"name" => $this->request->getPost("first_name"),
				"lname" => $this->request->getPost("last_name"),
				"address" => $this->request->getPost("address"),
				"latitude" => $this->request->getPost("lat"),
				"longitude" => $this->request->getPost("long"),
				"living_in" => $this->request->getPost("living_in"),
				"nationality" => $this->request->getPost("nationality"),
				"gender" => $this->request->getPost("gender"), //M: Male, F: Female
				"interestedin" => $this->request->getPost("looking_for")
			), $user_id);
			
		$users->edit_attributes(
			array(
				"age" => $this->request->getPost("age"),
				"relationship_status" => $this->request->getPost("relationship_status"),
				"relationship_looking_for" => $this->request->getPost("relationship_looking_for"),
				"religion" => $this->request->getPost("religion"),
				"body_type" => $this->request->getPost("body_type"),
				"height" => $this->request->getPost("height"),
				"job_status" => $this->request->getPost("job_status"),
				"education" => $this->request->getPost("education"),
				"housing" => $this->request->getPost("housing"),
				"drink" => $this->request->getPost("drink"),
				"smoke" => $this->request->getPost("smoke")
			),$user_id);
			
			$users->edit_seekattributes(
			array(
				"age" => $this->request->getPost("seeking_age"),
				 "relationship_status" => $this->request->getPost("relationship_status_looking_for"),
				"relationship" => $this->request->getPost("relationship_looking_for"),
				"nationality" => $this->request->getPost("seeking_nationality"),
				"living_in" => $this->request->getPost("seeking_living_in"),
				"body_type" => $this->request->getPost("seeking_bodytype"),
				"height" => $this->request->getPost("seeking_height"),
				"job_status" => $this->request->getPost("seeking_job_status"),
				"education" => $this->request->getPost("seeking_education"),
				"living_situation" => $this->request->getPost("seeking_living_situation"),
				"drink" => $this->request->getPost("seeking_drink"),
				"smoke" => $this->request->getPost("seeking_smoke"),
				"describe_yourself" => $this->request->getPost("describe_yourself"),
				"religion" => $this->request->getPost("seeking_religion")
				
			),$user_id);
			
		$res = $users->delete_user_images($user_id,$user_data[0]['profile_imageid']);
			//echo $users->getLastQuery();
			//die();
			$i  = 1;
			while($i){
				$photofile = $this->request->getFile('Photo'.$i);
				//echo $photofile;
				if($photofile){
					if($photofile->isValid()){
						$photopath = $photofile->store();
							
						if($photopath != ""){
						$photoid = $users->add_image(array(
							"image_path" => $photopath,
							"userid" => $user_id,
							"image_type" => "gallery"
						));
						
						}		
					}
				}
				else {
					break;
				}
				$i++;
				}
		
				$profile_img = $this->request->getFile('profile_image');
				if(!empty($profile_img)){
					if($profile_img->isValid()){
						$profile_imgpath = $profile_img->store();
							
						if($profile_imgpath != ""){
						$photoid1 = $users->add_image(array(
							"image_path" => $profile_imgpath,
							"userid" => $user_id,
							"image_type" => "Profile"
						));
						}

						if($photoid1 > 0){
							$res = $users->delete_user_profile($user_data[0]['profile_imageid']);
							// save and use as profile image
							$users->crud_update(array("profile_imageid" => $photoid1),$user_id);
						} 
					
					}
				}
				
			
			
		
			$returndata['status'] = "success";
			$returndata['message'] = "User Profile updated";
			
		}else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		
		return $this->respond($returndata,200);
	}

	/* -------------set subscription plan --------- */
	public function set_subscription_plan()
	{
		$returndata = array();
		$user_id = $this->request->getPost('user_id');
		$users = new Users();
		$user_data = $users->crud_read($user_id);
		//print_r($user_data);
		//die();
		if(!empty($user_data)){
			
			$users->crud_update(array(
				"planid" => $this->request->getPost("planid")
			), $user_id);
			
			$returndata['status'] = "success";
			$returndata['message'] = "User subsription Plan updated";
			
		}else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		
		return $this->respond($returndata,200);
	}
	
	
	/*-------------Get User Profile Data API---------------*/
	public function get_user_profile()
	{
		$returndata = array();
		$user_id = $this->request->getPost('user_id');
		$users = new Users();
		$user_data = $users->crud_read($user_id);
		
		if(!empty($user_data)){
			foreach ($user_data as $user) {
				
				$user_attributes = $users->read_attributes($user['user_id']);
				$user_seek_attributes = $users->read_seek_attributes($user['user_id']);
				$user_images = $users->read_images($user['user_id']);
				$new_user_img =array();
				
				foreach($user_images as $key=>$val)
				{
					if($val['image_id']== $user['profile_imageid'])
					{
						//echo $val['image_id'];
					}else{
						$new_user_img[$key] = $val; 
					}
				}
				
				$user_images = $new_user_img;
				
				
				$user_profileimage = $users->read_images('', $user['profile_imageid']);
				$returndata['user_profile_data'][] = array(
						"first_name" => $user['name'],
						"last_name" => $user['lname'],
						"user_email" => $user['user_email'],
						"user_mobile" => $user['mobile'],
						"user_country_code" => $user['country_code'],
						"user_address" => $user['address'],
						"user_gender" => $user['gender'],
						"user_latitude" => $user['latitude'],
						"user_longitude" => $user['longitude'],
						"user_active_status" => $user['is_active'],
						"user_living_in" => $user['living_in'],
						"user_nationality" => $user['nationality'],
						"user_looking_for" => $user['interestedin'],
						"verified" => $user['verified'],
						"user_last_online" => $user['last_online'],
						"user_online_status" => $user['online_status']
					);
					
				
				foreach ($user_attributes as $attributes) {
					$returndata['user_attributes'][] = array(
							"age" => $attributes['age'],
							"height" => $attributes['height'],
							"body_type" => $attributes['body_type'],
							"smoke" => $attributes['smoke'],
							"drink" => $attributes['drink'],
							"relationship_status" => $attributes['relationship_status'],
							"relationship_looking_for" => $attributes['relationship_looking_for'],
							"religion" => $attributes['religion'],
							"housing" => $attributes['housing'],
							"education" => $attributes['education'],
							"job_status" => $attributes['job_status']
						);
				}
				
				foreach ($user_profileimage as $profileimage) {
					$returndata['user_profileimage'][] = array(
							"profile_image" => base_url()."/writable/uploads/".$profileimage['image_path']
						);
				}
				foreach ($user_images as $images) {
					$returndata['user_images'][] = array(
							"image" => base_url()."/writable/uploads/".$images['image_path']
						);
				}
				//echo "<pre>";
				//print_r($user_seek_attributes);
				
				foreach ($user_seek_attributes as $seek_attributes) {
					$returndata['user_seek_attributes'][] = array(
							"seeking_age" => $seek_attributes['age'],
							"seeking_relationship_status" => $seek_attributes['relationship_status'],
							"seeking_relationship" => $seek_attributes['relationship'],
							"seeking_nationality" => $seek_attributes['nationality'],
							"seeking_living_in" => $seek_attributes['living_in'],
							"seeking_body_type" => $seek_attributes['body_type'],
							"seeking_height" => $seek_attributes['height'],
							"seeking_job_status" => $seek_attributes['job_status'],
							"seeking_education" => $seek_attributes['education'],
							"seeking_living_situation" => $seek_attributes['living_situation'],
							"seeking_drink" => $seek_attributes['drink'],
							"seeking_smoke" => $seek_attributes['smoke'],
							"seeking_religion" => $seek_attributes['religion'],
							"describe_yourself" => $seek_attributes['describe_yourself']
							
						);
				}
			}
			$returndata['status'] = "success";
			$returndata['message'] = "User Profile Data";
		} 
		else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		
		
		return $this->respond($returndata,200);
	}
	
	/*-------------Attributes List API---------------*/
	public function get_user_attributes()
	{
		$returndata = array();
		$users = new Users();
		$userid = $this->request->getPost('user_id');
		$returndata['attributes'] = $users->read_attributes($userid);
		$returndata['status'] = "success";
		$returndata['message'] = "Attributes list";
		return $this->respond($returndata,200);
	}

	/*-------------Contact support submit API---------------*/
	public function submit_contact_query()
	{
		$returndata = array();
		$contactus = new Contactus();
		$user_id = $this->request->getPost("user_id");
		$name = $this->request->getPost("name");
		$email = $this->request->getPost("email");
		$message = $this->request->getPost("message");
		if(!empty($user_id)){
			$contactus->crud_create(array(
				"userid" => $user_id,
				"name" => $name,
				"email" => $email,
				"message" => $message
			));
			$returndata['status'] = "success";
			$returndata['message'] = "Conatct query submitted";
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid user";
		}
		return $this->respond($returndata,200);
	}

	/*-------------Get Home Data API---------------*/
	public function get_home_data()
	{
		$returndata = array();
		$data =array();
		$today_date = date('Y-m-d');
		$event = new Event();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		
		$data['latitude'] = $this->request->getPost("lat");
		$data['longitude'] = $this->request->getPost("long");
		$data['min_range'] = $this->request->getPost("min_range");
		$data['max_range'] = $this->request->getPost("max_range");
		$data['unit'] = $this->request->getPost("unit");
		$user_seekattributes = $users->crud_read_seekattributes($user_id);
		$userdata = $users->crud_read($user_id);
		foreach($user_seekattributes as $seekattributes)
		{
			
			
			//$data['userid'] = $seekattributes['userid'];
			if(!empty($seekattributes['age']) && $seekattributes['age'] != 0)
			{
				$data['age'] = $seekattributes['age'];
			}
			
			if(!empty($seekattributes['height']) && $seekattributes['height'] != 0)
			{
				$data['height'] = $seekattributes['height'];
			}
			
			if(!empty($seekattributes['body_type'])){
				$data['body_type'] = $seekattributes['body_type'];
			}
			if(!empty($seekattributes['smoke'])){
				$data['smoke'] = $seekattributes['smoke'];
			}
			
			if(!empty($seekattributes['drink'])){
				$data['drink'] = $seekattributes['drink'];
			}
			if(!empty($seekattributes['relationship'])){
				$data['relationship'] = $seekattributes['relationship'];
			}
			
			if(!empty($seekattributes['relationship_status'])){
				$data['relationship_status'] = $seekattributes['relationship_status'];
			}
			
			if(!empty($seekattributes['religion'])){
				$data['religion'] = $seekattributes['religion'];
			}
			
			if(!empty($seekattributes['nationality'])){
				$data['nationality'] = $seekattributes['nationality'];
			}
			if(!empty($seekattributes['living_in'])){
				$data['living_in'] = $seekattributes['living_in'];
			}
			
			if(!empty($seekattributes['job_status'])){
				$data['job_status'] = $seekattributes['job_status'];
			}
			
			if(!empty($seekattributes['education'])){
				$data['education'] = $seekattributes['education'];
			}
			
			if(!empty($seekattributes['living_situation'])){
				$data['living_situation'] = $seekattributes['living_situation'];
			}
			
		}
						
		if(!empty($user_id)){
			
				$user_data = $users->read_home_data($data);
				$event_filter = $event->get_home_filter_data($data);
				//echo $event->getLastQuery();
				//echo "<pre>";
				//print_r($event_filter);
				//echo "<br/>";
				//die();
				if(!empty($event_filter)){
					
				$event_attending = $event->read_event_attending_home($events['event_id'],$user_id);
				
				$fav_event_data = $event->read_favorite_list($user_id, $events['event_id']);
				//print_r($fav_event_data);die;
				$event_images = $event->read_event_images($events['event_id']);
				$user_data = $users->crud_read($events['userid']);
				/* print_r($user_data);
				die(); */
				if(!empty($user_data))
				{
					$user_profileimage = $users->read_images('', $user_data[0]['profile_imageid']);
				}else{
					$user_profileimage ="";
				}
				
				$eventImages = array();
				foreach ($event_images as $image) {
					$eventImages[] = array(
						"event_images" => base_url()."/writable/uploads/".$image['event_image_path']
					);
				}
				if(!empty($user_profileimage))
				{
					$user_profileimage = base_url()."/writable/uploads/".$user_profileimage[0]['image_path'];
				}
				else{
					$user_profileimage ="";
				}
				$returndata['event_data'][] = array(
						"user_id" => $events['userid'],
						"event_id" => $events['event_id'],
						"event_title" => $events['event_title'],
						"event_description" => $events['event_description'],
						"favorite_status" => $fav_event_data ? $fav_event_data[0]['favorite_status'] : '',
						"confirmed_status" => $event_attending ? $event_attending[0]['confirmed'] : '',
						"reg_type" => $event_attending ? $event_attending[0]['reg_type'] : '',
						"profile_image" => $user_profileimage,
						"event_images" => $eventImages
					);
			}else{
				continue;
			}
					
					
				}
				if(!empty($returndata['event_data']))
				{
					$returndata['status'] = "success";
					$returndata['message'] = "Filter User details";
				}else{
					$returndata['status'] = "success";
					$returndata['message'] = "No data found";
				}
				
				
			}
			else {
				$returndata['status'] = "error";
				$returndata['message'] = "No data found";
			}
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid parameters";
		}
		
		return $this->respond($returndata,200);
	}
	
	
	/*-------------Filter API---------------*/
 	public function user_filter()
	{
		
		$returndata = array();
		$data = array();
		$data['user_id'] = $this->request->getPost('user_id');
		if(!empty($this->request->getPost('event_date_from')))
		{
			$data['event_date_from'] = date("Y-m-d", strtotime($this->request->getPost('event_date_from')));
		}else{
			$data['event_date_from'] = "";
		}
		
		if(!empty($this->request->getPost('event_date_to')))
		{
			$data['event_date_to'] = date("Y-m-d", strtotime($this->request->getPost('event_date_to')));
		}else{
			$data['event_date_to'] = "";
		}
		//echo $this->request->getPost('advance_body_type');
		//die();
		$data['event_type'] = $this->request->getPost('event_type');
		$data['location_radius_in_miles'] = $this->request->getPost('location_radius_in_miles');
		$data['interested_in'] = $this->request->getPost('interested_in');
		$data['age_from'] = $this->request->getPost('age_from');
		$data['age_to'] = $this->request->getPost('age_to');
		$data['event_lat'] = $this->request->getPost('event_lat');
		$data['event_long'] = $this->request->getPost('event_long');
		$data['verified_themselves'] = $this->request->getPost('verified_themselves');
		$data['advance_height_from'] = $this->request->getPost('advance_height_from');
		$data['advance_height_to'] = $this->request->getPost('advance_height_to');
		$data['advance_body_type'] = str_replace( '\/', '/', $this->request->getPost('advance_body_type'));
		$data['advance_education'] = str_replace( '\/', '/', $this->request->getPost('advance_education'));
		$data['advance_drink'] = str_replace( '\/', '/', $this->request->getPost('advance_drink'));
		$data['advance_smoke'] = str_replace( '\/', '/', $this->request->getPost('advance_smoke'));
		$data['advance_religion'] = str_replace( '\/', '/', $this->request->getPost('advance_religion'));
		$data['advance_relationship'] = str_replace( '\/', '/', $this->request->getPost('advance_relationship'));
		$data['advance_children'] = str_replace( '\/', '/', $this->request->getPost('advance_children'));
		$event = new Event();
		$users = new Users();
		if(!empty($data['user_id'])){
			
			//$user_seekattributes = $users->crud_read_seekattributes($data['user_id']);
			
			$event_details = $event->get_filter_event_data($data);
			
			//echo $event->getLastQuery();die;
			if(!empty($event_details)){
				foreach ($event_details as $events) {
					$data['userid'] = $events['userid'];
					
					$event_filter = $event->get_filter_data($data);
					//echo $event->getLastQuery();
					//echo "<pre>";
					//print_r($event_filter);
					//die();
					if(!empty($event_filter))
					{
					$event_attending = $event->read_event_attending_home($events['event_id']);
					$fav_event_data = $event->read_favorite_list($data['user_id'], $events['event_id']);
					//print_r($fav_event_data);die;
					$event_images = $event->read_event_images($events['event_id']);
					//$user_data = $users->crud_read($events['userid']); old
					
					//$user_data = $users->read_filter_seekattributes($events['userid'],$data);
					
					
						$user_profileimage = $users->read_images('', $user_data[0]['profile_imageid']);
						$eventImages = array();
						foreach ($event_images as $image) {
							$eventImages[] = array(
								"event_images" => base_url()."/writable/uploads/".$image['event_image_path']
							);
						}
						$returndata['event_data'][] = array(
								"user_id" => $events['userid'],
								"event_id" => $events['event_id'],
								"event_title" => $events['event_title'],
								"event_description" => $events['event_description'],
								"favorite_status" => $fav_event_data ? $fav_event_data[0]['favorite_status'] : '',
								"reg_type" => $event_attending ? $event_attending[0]['reg_type'] : '',
								"confirmed_status" => $event_attending ? $event_attending[0]['confirmed'] : '',
								"profile_image" => base_url()."/writable/uploads/".$user_profileimage[0]['image_path'],
								"event_images" => $eventImages
							);
					}else{
						continue;
					}
				}
				if(!empty($returndata['event_data']))
				{
					$returndata['status'] = "success";
					$returndata['message'] = "Event details";
				}else{
					$returndata['status'] = "success";
					$returndata['message'] = "No data found";
				}
			}
			else {
				$returndata['status'] = "error";
				$returndata['message'] = "No data found";
			}
		
		}else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		
		return $this->respond($returndata,200);
	}
	
		/*-------------Get Friends Profile Data API---------------*/
	public function get_friends_profile()
	{
		$returndata = array();
		$user_id = $this->request->getPost('user_id');
		$friend_id = $this->request->getPost('friend_id');
		$users = new Users();
		$user_data = $users->crud_read($friend_id);
	
		if(!empty($user_data)){
			foreach ($user_data as $user) {
				$user_attributes = $users->read_attributes($user['user_id']);
				$user_interests = $users->read_interest($user['user_id']);
				$user_seek_attributes = $users->read_seek_attributes($user['user_id']);
				$user_images = $users->read_images($user['user_id']);
				$new_user_img =array();
				foreach($user_images as $key=>$val)
				{
					if($val['image_id']== $user['profile_imageid'])
					{
						//echo $val['image_id'];
					}else{
						$new_user_img[$key] = $val; 
					}
				}
				
				//echo "<pre>";
				//print_r($new_user_img);
				//die();
				$user_images = $new_user_img;
				$user_profileimage = $users->read_images('', $user['profile_imageid']);
				$returndata['friend_profile_data'][] = array(
						"first_name" => $user['name'],
						"last_name" => $user['lname'],
						"user_address" => $user['address'],
						"user_gender" => $user['gender'],
						"user_bio" => $user['bio'],
						"last_online" => $user['last_online'],
						"user_verified" => $user['verified'],
						"user_instagram" => $user['instagram'],
					);
				foreach ($user_interests as $interests) {
					$returndata['friend_interests'][] = array(
							"interest" => $interests['interest_name']
						);
				}
				foreach ($user_profileimage as $profileimage) {
					$returndata['friend_profileimage'][] = array(
							"profile_image" => base_url()."/writable/uploads/".$profileimage['image_path']
						);
				}
				foreach ($user_images as $images) {
					$returndata['friend_images'][] = array(
							"image" => base_url()."/writable/uploads/".$images['image_path']
						);
				}
			}
			$returndata['status'] = "success";
			$returndata['message'] = "User Profile Data";
		}
		else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		
		return $this->respond($returndata,200);
	}

	/*-------------Set Online status API---------------*/
	public function online_status()
	{
		$returndata = array();
		
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		$datetime = date("Y-m-d H:i:s");
		if(!empty($user_id)){
			
			$user_data = $users->update_online_status(array(
					"last_online" => $datetime
				),$user_id);
				
			$returndata['status'] = "success";
			$returndata['message'] = "Online status updated";
			
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid user_id";
		}
		
		return $this->respond($returndata,200);
	}
	
	public function getTimezone()
	{
		$returndata =array();
		$str = array("America/Adak @ -09:00","America/Anchorage @ -08:00","America/Anguilla @ -04:00","America/Antigua @ -04:00","America/Araguaina @ -03:00","America/Argentina/Buenos_Aires @ -03:00","America/Argentina/Catamarca @ -03:00","America/Argentina/Cordoba @ -03:00","America/Argentina/Jujuy @ -03:00","America/Argentina/La_Rioja @ -03:00","America/Argentina/Mendoza @ -03:00","America/Argentina/Rio_Gallegos @ -03:00","America/Argentina/Salta @ -03:00","America/Argentina/San_Juan @ -03:00","America/Argentina/San_Luis @ -03:00","America/Argentina/Tucuman @ -03:00","America/Argentina/Ushuaia @ -03:00","America/Aruba @ -04:00","America/Asuncion @ -03:00","America/Atikokan @ -05:00","America/Bahia @ -03:00","America/Bahia_Banderas @ -05:00","America/Barbados @ -04:00","America/Belem @ -03:00","America/Belize @ -06:00","America/Blanc-Sablon @ -04:00","America/Boa_Vista @ -04:00","America/Bogota @ -05:00","America/Boise @ -06:00","America/Cambridge_Bay @ -06:00","America/Campo_Grande @ -04:00","America/Cancun @ -05:00","America/Caracas @ -04:00","America/Cayenne @ -03:00","America/Cayman @ -05:00","America/Chicago @ -05:00","America/Chihuahua @ -06:00","America/Costa_Rica @ -06:00","America/Creston @ -07:00","America/Cuiaba @ -04:00","America/Curacao @ -04:00","America/Danmarkshavn @ +00:00","America/Dawson @ -07:00","America/Dawson_Creek @ -07:00","America/Denver @ -06:00","America/Detroit @ -04:00","America/Dominica @ -04:00","America/Edmonton @ -06:00","America/Eirunepe @ -05:00","America/El_Salvador @ -06:00","America/Fort_Nelson @ -07:00","America/Fortaleza @ -03:00","America/Glace_Bay @ -03:00","America/Goose_Bay @ -03:00","America/Grand_Turk @ -04:00","America/Grenada @ -04:00","America/Guadeloupe @ -04:00","America/Guatemala @ -06:00","America/Guayaquil @ -05:00","America/Guyana @ -04:00","America/Halifax @ -03:00","America/Havana @ -04:00","America/Hermosillo @ -07:00","America/Indiana/Indianapolis @ -04:00","America/Indiana/Knox @ -05:00","America/Indiana/Marengo @ -04:00","America/Indiana/Petersburg @ -04:00","America/Indiana/Tell_City @ -05:00","America/Indiana/Vevay @ -04:00","America/Indiana/Vincennes @ -04:00","America/Indiana/Winamac @ -04:00","America/Inuvik @ -06:00","America/Iqaluit @ -04:00","America/Jamaica @ -05:00","America/Juneau @ -08:00","America/Kentucky/Louisville @ -04:00","America/Kentucky/Monticello @ -04:00","America/Kralendijk @ -04:00","America/La_Paz @ -04:00","America/Lima @ -05:00","America/Los_Angeles @ -07:00","America/Lower_Princes @ -04:00","America/Maceio @ -03:00","America/Managua @ -06:00","America/Manaus @ -04:00","America/Marigot @ -04:00","America/Martinique @ -04:00","America/Matamoros @ -05:00","America/Mazatlan @ -06:00","America/Menominee @ -05:00","America/Merida @ -05:00","America/Metlakatla @ -08:00","America/Mexico_City @ -05:00","America/Miquelon @ -02:00","America/Moncton @ -03:00","America/Monterrey @ -05:00","America/Montevideo @ -03:00","America/Montserrat @ -04:00","America/Nassau @ -04:00","America/New_York @ -04:00","America/Nipigon @ -04:00","America/Nome @ -08:00","America/Noronha @ -02:00","America/North_Dakota/Beulah @ -05:00","America/North_Dakota/Center @ -05:00","America/North_Dakota/New_Salem @ -05:00","America/Nuuk @ -02:00","America/Ojinaga @ -06:00","America/Panama @ -05:00","America/Pangnirtung @ -04:00","America/Paramaribo @ -03:00","America/Phoenix @ -07:00","America/Port-au-Prince @ -04:00","America/Port_of_Spain @ -04:00","America/Porto_Velho @ -04:00","America/Puerto_Rico @ -04:00","America/Punta_Arenas @ -03:00","America/Rainy_River @ -05:00","America/Rankin_Inlet @ -05:00","America/Recife @ -03:00","America/Regina @ -06:00","America/Resolute @ -05:00","America/Rio_Branco @ -05:00","America/Santarem @ -03:00","America/Santiago @ -03:00","America/Santo_Domingo @ -04:00","America/Sao_Paulo @ -03:00","America/Scoresbysund @ +00:00","America/Sitka @ -08:00","America/St_Barthelemy @ -04:00","America/St_Johns @ -02:30","America/St_Kitts @ -04:00","America/St_Lucia @ -04:00","America/St_Thomas @ -04:00","America/St_Vincent @ -04:00","America/Swift_Current @ -06:00","America/Tegucigalpa @ -06:00","America/Thule @ -03:00","America/Thunder_Bay @ -04:00","America/Tijuana @ -07:00","America/Toronto @ -04:00","America/Tortola @ -04:00","America/Vancouver @ -07:00","America/Whitehorse @ -07:00","America/Winnipeg @ -05:00","America/Yakutat @ -08:00","America/Yellowknife @ -06:00");
		$returndata['America_timezone'] = $str; 
		$returndata['status'] = "success";
		$returndata['message'] = "America Timezone List";
		return $this->respond($returndata,200);
		 
	}
	
	public function setUserStatus()
	{
		$returndata = array();
		$datetime = date("Y-m-d H:i:s");
		$users = new Users();
		$userdata = $users->crud_read();
		//print_r($userdata);
		foreach($userdata as $user)
		{
			$diff = strtotime($datetime) - strtotime($user['last_online']);

			$days    = floor($diff / 86400);
			$hours   = floor(($diff - ($days * 86400)) / 3600);
			$minutes = floor(($diff - ($days * 86400) - ($hours * 3600))/60);
			$seconds = floor(($diff - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
			
			//echo "currenttime ".$datetime."lastonline " . $user['last_online']. "diff  ".$seconds;
			//die(); 

			/*  if($user['last_online']< $datetime )
			{
				
			}  */
		}
		/* $user_id = $this->request->getPost("user_id");
		
		if(!empty($user_id)){
			
			$user_data = $users->update_online_status(array(
					"last_online" => $datetime
				),$user_id);
				
			$returndata['status'] = "success";
			$returndata['message'] = "Online status updated";
			
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid user_id";
		}
		
		return $this->respond($returndata,200); */
		
	}
	
}